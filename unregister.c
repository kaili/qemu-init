/********************************************************************************
 * unregister (linkable object file)                                            *
 *                                                                              *
 *  Author: Kai Peter, ©2016-2021                                               *
 * License: ISC                                                                 *
 * Version: 0.6                                                                 *
 *                                                                              *
 * Description: Unregister a virtual machine from qemu-init.                    *
 *******************************************************************************/
#define ME "unregister"

#include <unistd.h>
#include "buffer.h"
#include "case.h"
#include "errmsg.h"
#include "readclose.h"
#include "qstrings.h"
#include "open.h"
#include "wait.h"

#include "convertname.h"
#include "register.h"

#include "layout.h"

extern int hasAutostart();  /* don't include 'autostart.h' here (gcc warnings) */
extern int IsRunning();

int doUnregister(void) {
  int fd;
  int pos;
  stralloc sa = {0};      /* hold the whole regfile */
  stralloc ln = {0};      /* one line of regfile */
  buffer b;
  char buf[128];
  char *x;

  if (IsRegistered(&lnam) == 0)
    errint(EINVAL,B("Not a registered virtual machine: ",name.s));

  if (IsRunning(&lnam))
    errint(ESOFT,B("Please shut down ",lnam.s," first!"));

  if(hasAutostart(lnam.s) == 1) {    /* remove from autostart if enabled */
    int wstat;
    if(chdir(libdir) == -1) errsys(errno);
    int pid = fork();
    char *c[] = { "./qemud-ast", lnam.s, 0};
    if(!pid)
      if(execvp(*c,c) < 0)
        errint(errno,B("Unable to disable auostart"));
    wait_pid(&wstat,pid);
    /* something went wrong: perhaps insufficient privileges? */
    if(hasAutostart(lnam.s) == 1)
      errint(errno,B("Unable to disable autostart"));
  }

  if (openreadclose(regfile,&sa,64) != 1)
    errint(errno,B("Unable to read ",regfile,"!"));
  fd = open_trunc(regfile);
  if (fd < 0) errint(errno,B("Couldn't open ",regfile,"!"));
  buffer_init(&b,write,fd,buf,sizeof(buf));

  for(;;) {     /* loop through the temp. stralloc */
    pos = byte_chr(sa.s,sa.len,'\n');
    /* save the top/first line in stralloc 'ln' */
    if (!stralloc_copys(&ln,strsplit(sa.s,pos,'L'))) errmem;
    if (!stralloc_0(&ln)) errmem;

    /* remove top/first line from regfile's stralloc 'sa' */
    if (!stralloc_copys(&sa,strsplit(sa.s,pos,'R'))) errmem;
    if (!stralloc_0(&sa)) errmem;

    pos = byte_rchr(ln.s,ln.len,'/');
    x = strsplit(ln.s,pos,'R');
    case_lowers(x);       /* XXX: make sure it is in lower case */

    /* write back to regfile */
    if (str_diff(x,lnam.s)) {
      buffer_puts(&b,ln.s);
      buffer_puts(&b,"\n");
      buffer_flush(&b);
    }
    if (sa.len <= 1) break;
  }
  close(fd);
  stralloc_free(&ln);
  stralloc_free(&sa);
  out(B("Succesfully unregistered ",name.s,"."));
  return(0);
}
