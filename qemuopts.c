/********************************************************************************
 * qemuopts (linkable object file)                                              *
 *                                                                              *
 *  Author: Kai Peter, ©2020-                                                   *
 * License: ISC                                                                 *
 * Version: 0.9                                                                 *
 *                                                                              *
 * Description: Set standard options for qemu command line from rc-file.        *
 *******************************************************************************/
#define ME "qemuopts"

#include <unistd.h>
#include "case.h"
#include "env.h"
#include "errmsg.h"
#include "qstrings.h"
#include "scan.h"

#include "convbool.h"
#include "convertname.h"
#include "qemuopts.h"
#include "readconf.h"

/* Set options for qemu from rc-file */
void setStdOpts(char *key, char *value) {
  if (str_len(value) < 1) return;
  if (strequal(key,"NAME")) {
    if (str_len(value) < 2) value = name.s;  /* fallback: a name is required! */
    if (!stralloc_copys(&optname," -name ")) errmem;
    /* 'value' is 0-terminated already! */
    if (!stralloc_cats(&optname,value)) errmem;
  }
  if (strequal(key,"DESC")) {
    if (!stralloc_copys(&optdesc," (")) errmem;
    if (!stralloc_cats(&optdesc,value)) errmem;
    if (!stralloc_cats(&optdesc,")")) errmem;
    if (!stralloc_0(&optdesc)) errmem;
  }
  case_lowers(value);  /* all values below should be lower case */
  if ((strequal(key,"TGTARCH"))) {
    if (!str_copy(&qemubin,&value)) errmem;
  }

  if (strequal(key,"RUNAS")) {
    if (instr(usropts.s,"-runas ") > 0) return;
    if (strequal(value,"root")) {
      if (strequal(value,env_get("USER"))) {
        /* set "forceroot" as a switch to be used later on */
        if (!stralloc_copys(&runas,"forceroot")) errmem;
        return;
      } else {
        stralloc_init(&runas);
        return;
      }
    }
    if (!stralloc_copys(&runas," -runas ")) errmem;
    if (!stralloc_cats(&runas,value)) errmem;
    if (!stralloc_0(&runas)) errmem;
  }

  if (strequal(key,"DAEMON")) {
    if (instr(usropts.s,"-daemonize ") > 0) return;
    if (convBool(value)) dmn = 1;
  }

  if (strequal(key,"KVM")) {
    if (instr(usropts.s,"-enable-kvm ") > 0) return;
    if (convBool(value)) { kvm = 1;
    }
  }

  if (strequal(key,"RAM")) {
    if (instr(usropts.s,"-m ") > 0) return;
    if (!stralloc_copys(&mem," -m ")) errmem;
    if (!stralloc_cats(&mem,value)) errmem;
    if (!stralloc_0(&mem)) errmem;
  }

  if (strequal(key,"DISPLAY")) {
    if (instr(usropts.s,"-display ") > 0) return;
    /* right now only 'vnc', 'none' or 'egl-headless' are allowed */
    switch (value[0]) {
      case 'v': if(strequal(value,"vnc")         ) break; return;
      case 'e': if(strequal(value,"egl-headless")) break; return;
      case 'n': if(strequal(value,"none")        ) break; return;
       default: return;
    }
    if (!stralloc_copys(&display," -display ")) errmem;
    if (!stralloc_cats(&display,value)) errmem;
  }

  if (strequal(key,"VNCKEYB")) {
    if (instr(usropts.s,"-k ") > 0) return;
    if (strequal(value,"en")) return;
    vnckeyb[4] = value[0];
    vnckeyb[5] = value[1];
  }

  /* deprecated options: don't read CPU, BOOT, LOCAL ...   */
  /* as of now we stay with the notation in 'qemu options' */
  if (verbosity) {
    if ((strequal(key,"CPU"))   ||
        (strequal(key,"LOCAL")) ||
        (strequal(key,"BOOT"))  ||
        (strequal(key,"MOUSE")) ||
        (strequal(key,"TARGET_ARCH")) ||
        (strequal(key,"QEMU_DISPLAY")))  {

        log(B("The key ",key," is deprecated!"));
    }
  }
  return;
}
