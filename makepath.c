/********************************************************************************
 * makepath (linkable object file)                                              *
 *                                                                              *
 *  Author: Kai Peter, ©2020-present                                            *
 * License: ISC                                                                 *
 * Version: 0.2                                                                 *
 *                                                                              *
 * Description: Create directories given by '*path' recursively.                *
 *******************************************************************************/
#define VERSION "0.1"

#include <unistd.h>
#include <sys/stat.h>
#include "qstrings.h"

int mkpath(char *path, mode_t mode) {
  stralloc sa = {0};
  int i;
  struct stat st;

  umask(022);
  for (i=1;i<str_len(path);i++) {         /* ignore path[0] (could be a slash) */
      if (path[i] == '/') {  /* create all pre-sub directories recursively ... */
      if (!stralloc_copys(&sa,strsplit(path,i,'L'))) errmem;
      if (!stralloc_0(&sa)) errmem;
      if (stat(sa.s,&st) != 0)
        if (mkdir(sa.s,mode) !=0) return(errno);
    }
  }
  /* The "last" folder, which is the _required_ one. Here, a separate 'stat()' is
     not required - this have to be done before calling 'makepath()'.          */
  if (mkdir(path,mode) !=0) return(errno);

  return(0);
}
