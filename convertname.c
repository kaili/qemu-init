/********************************************************************************
 * convertname.c (linkable object file)                                         *
 *                                                                              *
 *  Author: Kai Peter, ©2020-                                                   *
 * License: ISC                                                                 *
 * Version: 0.3                                                                 *
 *                                                                              *
 * Description: Split the positional argument 'name' into path (if required),   *
 *              the real name and convert the name into lower case letters.     *
 *******************************************************************************/
#define ME "convertname"

#include <unistd.h>
#include "case.h"
#include "errmsg.h"
#include "qstrings.h"

#include "convertname.h"

stralloc name = {0}; /* ** the 'name' like used by the qemu option '-name'     */
stralloc path = {0}; /* ++('name' and 'path' COULD contain upper case letters) */
stralloc lnam = {0}; /* the 'name' in lower case letters (for internal use)    */

int validateName(stralloc *sa) {
  char ch;
  int i = 0;

  /* cut all after and including the first dot */
  i = instr(sa->s,".");             /* 'i' is required in the 'strsplit' below */
  if (i > 0) {
    if (!stralloc_copys(sa,strsplit(sa->s,i-1,QLEFT))) errmem;
    if (!stralloc_0(sa)) errmem;
    stralloc_copys(&name,sa->s);      /*    'name' is now shorter, thus enough */
    stralloc_0(&name);                /* ++ memory was allocated already       */
  }

  if (str_len(sa->s) > 42) return -1;       /* by convention: max length is 42 */

  if (sa->s[str_len(sa->s)-1] == '0')       /* name must not end with "0"      */
    errint(EINVAL,"The name MUST NOT end with \"0\"!");

  for (i=0;i<str_len(sa->s);i++) {    /* check for non alphanumeric characters */
    ch = sa->s[i];
    if ((ch < '0') || (ch > 'z')) return -1;
    if ((ch > '9') && (ch < 'A')) return -1;
    if ((ch > 'Z') && (ch < 'a')) return -1;
  }
  return(0);
}

void convertName(stralloc *sa) {
  int pos;

  pos = byte_rchr(sa->s,sa->len,'/');
  /* first: the 'name' MUST NOT end with a trailing slash */
  if (pos == str_len(sa->s) - 1) errint(EINVAL,B(sa->s," (trailing slash)"));

  /* if argument contains a path than split it */
  if (pos < str_len(sa->s) - 1) {
    if (!path.len) {      /* the '-p' option wins (than '&path' is initialized */
      if (!stralloc_copys(&path,strsplit(sa->s,pos,QLEFT))) errmem; }
    if (!stralloc_copys(&name,strsplit(sa->s,pos,QRIGHT))) errmem;
    if (!stralloc_0(&name)) errmem;
  } else {
    if (!stralloc_copys(&name,sa->s)) errmem;
    if (!stralloc_0(&name)) errmem;
  }
  if (validateName(&name) == -1)
    errint(EINVAL,B("The name: ",name.s," is invalid!"));

  if (!stralloc_0(&path)) errmem;       /* important: terminate with '\0' here */

  /* Try to catch some other (user input) errors: */
  if (str_len(name.s) == 0) errint(EINVAL,sa->s);

  if (!stralloc_copy(&lnam,&name)) errmem;
  case_lowers(lnam.s);               /* convert 'name' into lower case letters */
}
