#********************************************************************************
# Administration of qemu virtual machines to use with qemu-init                 *
#                                                                               *
#  Author: Kai Peter, ©2016-present                                             *
# Version: 0.63.2                                                               *
# License: ISC                                                                  *
#                                                                               *
# Description: Control of all administrative functions of 'qemu-init'.          *
#********************************************************************************
VERSION=`cat $0 | grep -m 1 'Version' | cut -d\   -f3`       # get version number
ME=`basename $0`

PATH=QLIBDIR:`dirname $0`:$PATH
declare -i DBG=0

showHelp() {
  echo -e "Usage $ME <action> [ <name> ] [ <option> ]"
  echo -e ""
  echo -e "Actions:"
  echo -e "   $ME [ -a|-r|-u ] <name>" # [ -f ] [ -U ]"
  echo -e "   $ME [ -S|-D|-R|-m|-s|--config|--vnc ] <name>"
  echo -e "   $ME -c <command> <name>"
  echo -e "   $ME [ -l ] | [ -h ] | [ -V ]"
  echo -e ""
  echo -e "   -a | --autostart <name> \t change (revert) the AUTOSTART option"
  echo -e "   -c | --cmd <command> <name> \t run qemu monitor instruction <command> for <name>."
  echo -e "                           \t <command> have to be quoted properly if necessary."
  echo -e "   --config <name>         \t show configuration of virtual machines from rc file"
  echo -e "   -D | --stop <name>      \t stop virtual machine <name>"
  echo -e "   -h | --help             \t show this help screen"
  echo -e "   -l | --list             \t list all registered virtual machines"
  echo -e "   -m | --monitor <name>   \t connect to the qemu monitor via tcp"
  echo -e "   -r | --register <name>  \t register a virtual machine"
  echo -e "   --reset <name>          \t perform a warmstart of <name>"
  echo -e "   -R | --restart <name>   \t restart virtual machine <name>"
  echo -e "   -S | --start <name>     \t start virtual machine <name>"
  echo -e "   -s | --status <name>    \t show runtime configuration of <name>"
  echo -e "   -u | --unregister <name>\t unregister virtual machine <name>"
  echo -e "   -V | --version          \t show version info and exit"
  echo -e "   --vnc <name>            \t start vncviewer for <name>"
  echo -e ""
  echo -e "Options:"
  echo -e "   --cdrom [<file>]        \t eject cdrom drive and mount <file> to first cd drive"
  echo -e "                           \t w/o <file> the first cdrom will be ejected only"
  echo -e "   -f                      \t a file operation depending on action"
  echo -e "   -U                      \t a register a VM with user 'QUSR'"
  echo -e ""
}

error() {
  printf "\033[1merror: \033[0m"
  case $1 in
    1) echo "$ME requires an argument! Run '$ME -h' for help.";;
    3) echo "$ME: Invalid number of arguments.";;
  # monitor command issues
   30) echo "No cdrom drive found.";;
  esac
  exit $1
}

register() {
  local VMRC=$1
#  qemud-reg "$FLAG_U" -r $VMRC && return
  qemud-reg -r $VMRC && return
  exit $?

  # create hd image
  if [ "$FLAG_F" ] && [ ! -f "$TF/$SN-qemu.img" ] ; then
    local QEMU_IMG
    QEMU_IMG=`which qemu-img 2>DEVNULL`
    [ "$QEMU_IMG" ] && echo "creating disk image ..." && \
      ${QEMU_IMG} create -f qcow2 $TF/$SN-qemu.img 30G || \
      printf "\033[1mWarning: Couldn't create hd image!\033[0m\n"
  fi
}

unregister() {
  local NAME=$1 ; qemud-reg -u $NAME && return || exit $?
}
autostart() {
  # Important: 'qemud-ast' MUST be called with absolute path!
  local VMRC=$1 ; "QLIBDIR/qemud-ast" $VMRC && return || exit $?
}
#--------------------------------------------------------------------------------
#***** qemu monitor commands ****************************************************
cdrom(){
  local NAME=$1
  local DEVICE
  # we use the first cd drive which we found
  DEVICE=`qemud-sio $NAME -c "info block" | grep -m 1 '\-cd'`
  DEVICE=`echo $DEVICE | cut -d\  -f1 | sed 's/://g'`
  [ ! "$DEVICE" ] && error 30

  # eject cdrom, unset FLAG_CDROM to prevent 'invalid file warning' and return
  [ "$FLAG_EJECT" ] && qemud-sio -c "$NAME" "eject $DEVICE" >DEVNULL && \
      FLAG_CDROM="" && return

  qemud-sio -c "$NAME" "change $DEVICE $FLAG_CDROM" >DEVNULL
  [ "$DBG" -gt 0 ] && echo -n "CD: " && \
    qemud-sio -c "$NAME" "info block" | grep "^$DEVICE" | awk '{ print $3; };'
}
reset() {
  local NAME=$1

  # if FLAG_CDROM starts with a minus sign then it doesn't contain a file but the
  # next parameter - without an argument <file> it will be ejected.
  echo "$FLAG_CDROM" | grep -q '^\-' && FLAG_CDROM="eject"
  [ "$FLAG_CDROM" = "eject" ] && FLAG_EJECT="eject" && cdrom "$NAME"

  # insert <file> at the first cdrom if <file> exists
  [ -f "$FLAG_CDROM" ] && cdrom "$NAME"

  qemud-sio --cmd "system_reset" "$NAME"
  exit $?
}
#--------------------------------------------------------------------------------
#***** begin processing *********************************************************
[ ! "$#" -gt 0 ] && error 1          # no argument/parameter/option was specified

PARAMS=$@                  # a string with all command line options
OPTARG=($PARAMS)           # an array with all the arguments
idx=0                      # loop index counter
unset ACTION
unset DoExec

for ARG in $PARAMS ; do
  idx=`expr $idx + 1`        # increment the idx
  case "$ARG" in
    # internal actions
  -a|--autostart) ACTION="autostart ${OPTARG[idx]}";;
       -h|--help) showHelp ; exit 0;;
       -l|--list) ACTION="qemud-reg";;                  # list all registered VM's
   -r|--register) ACTION="register ${OPTARG[idx]}";;
         --reset) ACTION="reset ${OPTARG[idx]}";;       # perform a 'warm' start
 -u|--unregister) ACTION="unregister ${OPTARG[idx]}";;
    -V|--version) echo "$ME-$VERSION" ; exit 0;;
    # external actions
       -D|--stop) ACTION="qemud-init ${OPTARG[idx]} stop";;            # shutdown
      -S|--start) ACTION="qemud-init ${OPTARG[idx]} start";;           # start
        --config) ACTION="qemud-init ${OPTARG[idx]} config" ;;         # config
    -R|--restart) ACTION="qemud-init ${OPTARG[idx]} restart" ;;        # restart
     -s|--status) ACTION="qemud-init ${OPTARG[idx]} status";;          # status
    -m|--monitor) ACTION="qemud-sio --mon ${OPTARG[idx]}";;
        -c|--cmd) [ $# -ne 3 ] && error 3               # check number of options
                  qemud-sio --cmd "$2" "$3";;
           --vnc) ACTION="qemud-vnc ${OPTARG[idx]}" ;;              # vnc(viewer)
    # options (flags)
              -f) FLAG_F="f";;
          --test) echo "key $ARG value ${OPTARG[idx]}";;
         --cdrom) FLAG_CDROM="${OPTARG[idx]}"
                  [ ! "$FLAG_CDROM" ] && FLAG_CDROM="eject";;
              -U) FLAG_U="-U";;
  esac
  [ ! "$DoExec" ] && [ "$ACTION" ] && DoExec="$ACTION"    # the first ACTION wins
done
# simple warning for arguments ignored by 'qemu-init' or 'qemud-mon'
echo "$DoExec" | grep -q 'qemu-' && [ $# -gt 2 ] && \
   echo "Warning: ignoring gratuitous arguments!"

[ "$DBG" -gt 2 ] && echo "finished"
[ "$DoExec" ] && ${DoExec} ; exit $?
