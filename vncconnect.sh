#********************************************************************************
# Handle vnc ports configuration and vnc connections                            *
#                                                                               *
#  Author: Kai Peter, ©2019-present                                             *
# Version: 0.35                                                                 *
# License: ISC                                                                  *
#                                                                               *
# Description: A wrapper to invoke vncviewer for local registered vm's.         *
#              The situation around using different vncviewers is really weird, *
#              thus the usage of this wrapper for vnc connections is a required *
#              workaround.                                                      *
#********************************************************************************
VERSION=`cat $0 | grep -m 1 'Version' | cut -d\   -f3`       # get version number
ME=`basename $0`
FN="VNCFILE"

usage() { echo "Usage: $ME <name>" ; exit 0 ; }

connect() {
  local VNCHOST=`hostname`
  local P   # the vnc port

  # initiate a vnc connection (we put it into background w/o any output)
  [ -n "$NAME" ] && P=`grep "$NAME" "$FN" | cut -d: -f3` && [ -n "$P" ] && \
    exec vncviewer "$VNCHOST":"$P" &>DEVNULL & PID=$!

  # check and report error: wait 1 sec for the background process to start, then
  # check if the PID still exists and if not - something went wrong - simply ...
  sleep 1 && PID=`ps ux | grep $PID | grep -v 'grep' | awk '{ print $2; }'`
  # ... invoke 'vncviewer' in the foreground again to see the error message
  [ -z "$PID" ] && vncviewer "$VNCHOST":"$P"
}

[ ! "$1" ] && usage
NAME=`echo "$1" | tr '[A-Z]' '[a-z]'` && connect

exit $?
