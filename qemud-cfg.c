/********************************************************************************
 * qemud-cfg (executable file)                                                  *
 *                                                                              *
 *  Author: Kai Peter, ©2020-                                                   *
 * License: ISC                                                                 *
 * Version: 0.14                                                                *
 *                                                                              *
 * Description: Read the <name>-qemud.rc/<name>-qemu.cfg file(s) and build qemu *
 *              command line.                                                   *
 *******************************************************************************/
#define ME "qemud-cfg"
#define VERSION "0.12"

#include <unistd.h>
#include "buffer.h"
#include "getoptb.h"
#include "qstrings.h"

#include "convertname.h"
#include "getextoutput.h"
#include "isrunning.h"
#include "qemudctrls.h"
#include "qemuopts.h"
#include "readconf.h"
#include "usage.h"
#include "vncports.h"

#include "layout.h"
#include "ac-usrgrp.c"

stralloc cmd = {0};
int verbosity;

stralloc stdopts = {0};
stralloc usropts = {0};

stralloc optname = {0};
stralloc optdesc = {0};
stralloc tgtarch = {0};
stralloc runas = {0};

char *qemubin = "qemu-system-x86_64";   /* hard fallback! */

char cpu[] = " -cpu host";
int kvm;    /* default option "-enable-kvm" */
int dmn;    /* default option "-daemonize"  */

stralloc display = {0};
stralloc mem = {0};

char boot[] = " -boot order=dc,menu=off -no-fd-bootchk";
char vnckeyb[] = " -k en";
char vncmice[] = " -usb -device usb-tablet";

stralloc monitor = {0};
unsigned int vncport = 0;

void buildCmdline(int flagx) {
  setMonitor();

  if(!flagx)              /* for the cmdline we do not add the description ... */
    if (optdesc.len)      /* ... otherwise concatenate description to the name */
      if (!stralloc_cats(&optname,optdesc.s)) errmem;

  if (!stralloc_0(&optname)) errmem;

  if (!stralloc_copys(&cmd,qemubin)) errmem;
  if (!stralloc_cats(&cmd,optname.s)) errmem;

  /* we have to set runas here if not defined in rc-file */
  if (runas.len <= 1) {
    if (!stralloc_copys(&runas," -runas ")) errmem;
    if (!stralloc_cats(&runas,sysusr)) errmem;
    if (!stralloc_0(&runas)) errmem;
  }
  if (instr(runas.s,"forceroot") == 0)   /* "forceroot" is set in qemuopts */
    if (!stralloc_cats(&cmd,runas.s)) errmem;

  if (dmn) if (!stralloc_cats(&cmd," -daemonize")) errmem;
  if (kvm) if (!stralloc_cats(&cmd," -enable-kvm")) errmem;

  if(instr(usropts.s,"-cpu ") == 0)
    if (!stralloc_cats(&cmd,cpu)) errmem;     /* use default */
  if (instr(usropts.s,"-m ") == 0)
    if (!stralloc_cats(&cmd,mem.s)) errmem;

  if (!stralloc_cats(&cmd,boot)) errmem;
  /* the '&display' have to be completed here, because in the rc-file $VNCKEYB is
     set after $DISPLAY - thus we don't have a valid value in 'setStdOpts()'.  */
  if (display.len) {
    if (instr(display.s,"vnc") > 0) {
      if (!stralloc_cats(&display,"=:")) errmem;
      vncport = initvnc(lnam.s);
      if (!stralloc_cats(&display,fmtnum(vncport))) errmem;
      if (instr(usropts.s," -k ") == 0)     /* set default vnc keyboard layout */
        if (!stralloc_cats(&display,vnckeyb)) errmem;
      if (instr(usropts.s," -usb -device usb-tablet") == 0)    /* default mice */
        if (!stralloc_cats(&display,vncmice)) errmem;
    }
    if (!stralloc_0(&display)) errmem;
    if (!stralloc_cats(&cmd,display.s)) errmem;
  }
  if (!stralloc_0(&stdopts)) errmem;  /* now 'stdopts' is complete */
  if (stdopts.len > 0) {
    if (!stralloc_cats(&cmd,stdopts.s)) errmem;
  }

  if (usropts.len > 1) {
    if (!stralloc_cats(&cmd," ")) errmem;
    if (!stralloc_cats(&cmd,usropts.s)) errmem;;
  }

  if (!stralloc_cats(&cmd,monitor.s)) errmem;

  /* pidfile */
  if (!pidfile.len) errint(ESOFT,"No pidfile defined!");   /* shouldn't happen */
  if (!stralloc_cats(&cmd," -pidfile ")) errmem;
  if (!stralloc_cat(&cmd,&pidfile)) errmem;
  if (!stralloc_0(&cmd)) errmem;
}

int main(int argc, char **argv) {
  int opt;
  stralloc parg = {0};
  int flag_cmdln = 0;
  int flag_ctrls = 0;
  int flag_pidfn = 0;
  int flag_print = 0;
  int flag_rtcfg = 0;
  buffer b;
  char buf[1024];

  while ((opt = getopt(argc,argv,"cfprxvV")) != opteof)
  switch(opt) {
    case 'c': flag_ctrls = 1; break;
    case 'f': flag_pidfn = 1; break;
    case 'p': flag_print = 1; break;
    case 'r': flag_rtcfg = 1; break;   /* runtime configuration */
    case 'x': flag_cmdln = 1; break;
    case 'V': version(ME,VERSION); break;
    case 'v': verbosity++; break;
    default:  usage(B(ME," [ -p | -r ]")); break;
  }
  argc -= optind;
  argv += optind;

  if (!*argv) usage(B(ME," [ -p | -r ]"));
  if (!stralloc_copys(&parg,*argv)) errmem;
  if (!stralloc_0(&parg)) errmem;
  convertName(&parg);

  if (!IsRegistered(&lnam))
    errint(EINVAL,B("Not a registered virtual machine: ",name.s));

  if (IsRunning(lnam)) {   /* 'IsRunning' does initialize the pidfile name too */
    if (flag_rtcfg) {    /* By definition: don't print control parameters here */
       printConfig(proccmd,1);
       _exit(0);
    }
  }
  if(flag_rtcfg) {       /* not running, thus ...                 */
    flag_print = 1;      /* ... set flag 'p' to read config file  */
  }
  if (flag_print) flag_cmdln = 1;    /* '-p' requires the cmdline */
  if (readConfig() == -1) errint(errno,"Unable to read config file");

  if(flag_cmdln) buildCmdline(flag_cmdln);

  if (flag_print) { printConfig(cmd,0); _exit(0); }   /* print config and exit */

  if (flag_cmdln) {
    buffer_init(&b,write,4,buf,sizeof(buf));
    buffer_putsflush(&b,cmd.s);
  }
  buffer_init(&b,write,3,buf,sizeof(buf));
  if (flag_pidfn) {
    buffer_puts(&b,"PIDFILE=");
    buffer_puts(&b,pidfile.s);
    buffer_puts(&b,"\n");
  }
  if (flag_ctrls) {
    buffer_puts(&b,"BOOTDELAY=");
    buffer_puts(&b,fmtnum(bootdelay));
    buffer_puts(&b,"\n");
    buffer_puts(&b,"STOPDELAY=");
    buffer_puts(&b,fmtnum(stopdelay));
    buffer_puts(&b,"\n");
    buffer_puts(&b,"QEMUD_TARGET=");
    buffer_puts(&b,qemubin);
    buffer_puts(&b,"\n");
    buffer_puts(&b,"QEMUD_NAME=");
    buffer_puts(&b,lnam.s);
    buffer_puts(&b,"\n");
    buffer_puts(&b,"QEMUD_PATH=");
    buffer_puts(&b,path.s);
    buffer_puts(&b,"\n");
  }
  buffer_flush(&b);
  _exit(0);
}
