#ifndef CONVERTNAME_H
#define CONVERTNAME_H

extern stralloc name;  /* ** the 'name' like used by the qemu option '-name'     */
extern stralloc path;  /* ++('name' and 'path' COULD contain upper case letters) */
extern stralloc lnam;  /* the 'name' in lower case letters (for internal use)    */

extern char regfile[];

extern void convertName();
extern int validateName();
extern int IsRegistered();

#endif
