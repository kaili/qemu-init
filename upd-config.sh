#********************************************************************************
# put all non existing entries from 'qemu-init.reg' into the new configfiles

ETCDIR=QETCDIR
iFN="$ETCDIR/qemu-init.reg"

if [ ! -f "$iFN" ] ; then
  printf "Couldn't find \033[1m$iFN\033[0m.\n" && exit 1
fi

# regfile
oFN="$ETCDIR/registered"
while IFS= read L ; do
  tmp=`echo "$L" | grep -v ^# | cut -d: -f1 | cut -d- -f1`
  N=`basename "$tmp"`
  N=`grep -i "$N" "$oFN" 2>/dev/null`
  N=`basename "$N"`
  [ -z "$N" ] && echo "$tmp" >> "$oFN"
done < "$iFN"

# autostart
oFN="$ETCDIR/autostart"
while IFS= read L ; do
  AUTOSTART=`echo "$L" | cut -d: -f2 | tr '[A-Z]' '[a-z]'`
  [ "$AUTOSTART" = "y"   ] && AUTOSTART=1
  [ "$AUTOSTART" = "on"  ] && AUTOSTART=1
  [ "$AUTOSTART" = "yes" ] && AUTOSTART=1
  if [ "$AUTOSTART" = "1" ] ; then
    tmp=`echo "$L" | cut -d: -f1`
    tmp=`basename "$tmp" | cut -d- -f1`
    N=`grep -i "$tmp" "$oFN" 2>/dev/null`
    [ -z "$N" ] && echo "$tmp" >> "$oFN"
  fi
done < "$iFN"

# *-qemu.rc -> *-qemud.rc, *-user.cfg -> *-qemu.cfg
iFN="$ETCDIR/registered"
while IFS= read L ; do
  cd `dirname $L` 2>/dev/null
  if [ $? = 0 ] ; then
    tmp=`basename $L | tr '[A-Z]' '[a-z]'`
	[ -f $tmp-qemu.rc ]  && cp -n $tmp-qemu.rc  $tmp-qemud.rc 2>/dev/null
	[ -f $tmp-user.cfg ] && cp -n $tmp-user.cfg $tmp-qemu.cfg 2>/dev/null
  fi
done < "$iFN"
