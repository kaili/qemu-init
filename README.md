Some tools to make handling of multiple (headless) qemu VM's easier.

THIS SOFTWARE COMES WITH NO WARRANTY OF ANY KIND! (See LICENSE file too)

The most recent documentation can be found at https://qemu.qware.org.

