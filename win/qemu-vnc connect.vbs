'*******************************************************************************
' qemu-vnc connect - vb script                                                 *
'                                                                              *
'  Author: Kai Peter, ©2019-<present>                                          *
' Version: 0.34                                                                *
' License: ISC                                                                 *
'                                                                              *
' Description: This script will be called/included by the "qemu-vnc connect"   *
'              hta application.                                                *
'*******************************************************************************
Set objShell = CreateObject( "WScript.Shell" )            'get required env vars
Temp = objShell.ExpandEnvironmentStrings( "%Temp%" )
Home = objShell.ExpandEnvironmentStrings( "%USERPROFILE%" )
Set objShell = Nothing
'define global variables and objects
arrServer = array()
Dim File(2)                      'use an array for global file names, and assign
File(0) = Temp & "\qemu-vncports"                     '++ the data file name and
File(1) = Home & "\qemu-vnc.ini"                      '++ the config file name
Dim PrivKey
Dim sshUser
Dim Refresh

'*******************************************************************************
'read the config from file and assign the settings to variables
Function ReadConfig()
  Set fso = CreateObject("Scripting.FileSystemObject")
  If Not fso.FileExists(File(1)) Then
     fso.CopyFile "qemu-vnc.ini" , File(1)
     EditConfig()
  End If
  arrServer = array()            'important: make sure the array is really empty
  Set f = fso.OpenTextFile(File(1))
  Do
    Line = f.ReadLine
    If Line = "[Servers]" Then
      i = 0
      Do
        Line = f.ReadLine
        If Left(Line, 1) <> "#" Then
          'Add one element to arrServer and assign a value to it
          ReDim Preserve arrServer(UBound(arrServer) + 1)
          arrServer(UBound(arrServer)) = Line
          i = i + 1
        End If
        If Line = "" Then Exit Do
      Loop Until f.AtEndOfStream
    End If
    'Get the private key and the qemu-init user
    If Left(Line, 6) = "sshKey"  Then PrivKey = Right(Line,Len(Line) - 7)
    If Left(Line, 6) = "sshUsr"  Then sshUser = Right(Line,Len(Line) - 7)
    If Left(Line, 7) = "Refresh" Then Refresh = Right(Line,Len(Line) - 8)
  Loop Until f.AtEndOfStream
  f.Close
  Set fso = Nothing  '******* End read config **********************************
End Function

'*******************************************************************************
Function GetRemoteData(Value)
  f = File(0)
  Set fso = CreateObject("Scripting.FileSystemObject")
  If fso.FileExists(f) Then fso.DeleteFile(f)
  'clear the DropDown list (could contain elements) and set a "busy message"
  ClearDropDown()
  Set objOption = Document.createElement("OPTION")
  objOption.Text = "Retrieving data ..."
  DropDown.Add(objOption)

  'the handling of quotation marks and spaces is a bit starnge on Windows ...
  PrivKey = Chr(34) & Replace(Home & "\" & PrivKey, Chr(34), "") & Chr(34)
  sshUser = Replace(sshUser, Chr(34), "")
  Set objShell = CreateObject("WScript.Shell")
  For Each S in arrServer
    command = "plink -i " & PrivKey & " -l " & sshUser & " -x -batch " & S
'msgbox command
    'the "plink" takes much more time than writing to the file
    objShell.Run "cmd /c " & command & " >> " & f ,0 , True
  Next

  If Not fso.FileExists(f) Then
     'catch error if not or wrong servers are defined (in ini file)
     Call MsgBox ("Couldn't get data from server(s) - check config!" & _
          vbCrLf & vbCrLf & "", vbOK + vbCritical)
     'open the config file but don't restart
     objShell.Run "notepad.exe ""qemu-vnc.ini""" , 1, True
     Call MsgBox("Restart application to changes take effect.", _
                  vbOK + vbInformation, "Configuration")
     Self.Close   'close application to avoid an endless loop
  End If

  BuildDropDown()       '(re)build the DropDown list
  Set fso = Nothing
  Set objShell = Nothing
End Function

Sub Window_OnLoad     '*********************************************************
  ReadConfig()
  Set fso = CreateObject("Scripting.FileSystemObject")
  If Not fso.FileExists(File(0)) Then GetRemoteData("")
  'If "Refresh" is invalid or 0 then do not refresh
  If IsNumeric(Refresh) And Refresh > 0 Then
     Set f = fso.GetFile(File(0))
     'Refresh data from servers if older then "Refresh" (in minutes)
     If DateDiff("n",f.DateLastModified,Now()) > Refresh Then GetRemoteData("")
  End If
  Set fso = Nothing
  BuildDropDown()
End Sub

'*******************************************************************************
Function ClearDropDown()
  Set objOption = Document.createElement("OPTION")
  For Each objOption in DropDown.Options: objOption.RemoveNode: Next
End Function

Function BuildDropDown()
  ClearDropDown()         'clear the "Retrieving data ..." element if needed
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile(File(0), 1)
  Do Until f.AtEndOfStream
     Line = f.ReadLine
     Set objOption = Document.createElement("OPTION")
     If InStr(Line,":") > 1 Then
        arrMyField = Split(Line, ":")
        objOption.Value = arrMyField(0)
        objOption.Text =  arrMyField(0)      'the first element contains <name>
        DropDown.Add(objOption)
     End If
  Loop
  f.Close
  Set objOption = Nothing
  Set fso = Nothing
End Function

'*******************************************************************************
Sub btnOK_OnClick(Name)     '**** OK button ************************************
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set f = fso.OpenTextFile(File(0), 1)
  Do Until f.AtEndOfStream
     Line = f.ReadLine
     If InStr(Line,":") > 1 Then
        arrNameHostPort = Split(Line, ":")
        If arrNameHostPort(0) = Name Then
           host = arrNameHostPort(1)
           port = arrNameHostPort(2)
           Exit Do
        End If
     End If
  Loop
  f.Close
  Set fso = Nothing
  If host = "" Or port = "" Then Exit Sub

  Set objShell = CreateObject("WScript.Shell")
  'We can call the bundled tnviewer directly (located in the same folder)
  objShell.Exec "tvnviewer " & host & ":" & port
  Set objShell = Nothing
End Sub

Sub EditConfig       '**** Config button was pressed ***************************
  Set objShell = CreateObject("WScript.Shell")
  objShell.Run "notepad.exe " & File(1) , 1, True
  Set objShell = Nothing
  'Perhaps "qemu-vnc.reg" exists but contains none or invalid data
  Set fso = CreateObject("Scripting.FileSystemObject")
  If fso.FileExists(File(0)) Then fso.DeleteFile(File(0))
  Set fso = Nothing
  ReadConfig()
  GetRemoteData("")
End Sub

Sub btnCancel_OnClick()     '**** Cancel button was pressed ********************
  Window.Close
End Sub

Sub Document_OnKeyUp()    '**** ESC key was pressed --> close application ******
  intKeyCode = Window.Event.Keycode
  If intKeyCode = 27 Then Window.Close
End Sub
