You have to configure 'QEMU VNC connect' before it can be used. Please refer to

  http://qemu.qware.org/doku.php/tools:qemu-vnc-connect

to learn how to do it.  The config file 'qemu-vnc connect.ini' contains detailed
descriptions for each option also.


Existing user config files in %USERPROFILE% will not be touched. Thus new config
options have to be added by hand.  Alternative a user's config can be deleted to
force a fresh start. Below are newly added options per version:

Version 0.31: Refresh option
  'Refresh' defines the time before client data will be refreshed at start

