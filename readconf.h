#ifndef READCONF_H
#define READCONF_H

extern stralloc stdopts;         /* DSK, CDR and NIC's ... */
extern stralloc usropts;         /* options from <name>-qemu.cfg */

extern int verbosity;

extern int readConfig();
extern void setMonitor();
extern void printConfig();
extern void setStdOpts();
extern void getControls();
extern void getPeripherals();

#endif
