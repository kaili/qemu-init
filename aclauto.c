/********************************************************************************
 * aclauto (linkable object file)                                               *
 *                                                                              *
 *  Author: Kai Peter, ©2021-present                                            *
 * Version: 0.3                                                                 *
 * License: ISC                                                                 *
 *                                                                              *
 * Description: Set and remove the group 'w' bit on config file 'autostart'.    *
 *******************************************************************************/
#include <unistd.h>
#include <sys/stat.h>
#include "errmsg.h"
#include "qstrings.h"
#include "stralloc.h"
#include "wait.h"

#include "autostart.h"
#include "djbhash.h"
#include "getextoutput.h"
#include "layout.h"

#include "ac-sumprg.c"
#include "ac-psprg.c"

extern char hashast[];

int autoacls(stralloc *action,stralloc *phash) {
  stralloc sa = {0};
  struct stat st;
  int pid = 0;
  int wstat;

  unsigned long p = getppid();
  unsigned long x = djbhash(fmtnum(getppid())); /* create hash from parent pid */

  /* make sure 'qemu-acl' was called by a parent process */
  if(instr(phash->s,fmtnum(x)) == 0) {
    errint(ESOFT,"invalid verification token - aborting!"); return(-1); }

  /* verify: parent 'qemud-ast' _MUST_ be called with correct absolute path */
  if(!stralloc_copys(&sa,libdir)) errmem;
  if(!stralloc_cats(&sa,"/qemud-ast")) errmem;
  if(!stralloc_0(&sa)) errmem;
  char *s[] = { psprg, "-p", fmtnum(p), "-o", "command", NULL};
  getExtOutput(s);
  /* ... it seems */
  if (!instr(result.s,sa.s))
    errint(ESOFT,"either the absolute path or 'qemud-ast' itself is invalid!");

  /* now check that 'qemud-ast' of the parent process is unmodified */
  stralloc_init(&sa);
  if(!stralloc_copys(&sa,libdir)) errmem;
  if(!stralloc_cats(&sa,"/qemud-ast")) errmem;
  if(!stralloc_0(&sa)) errmem;
  s[0] = sumprg;     /* re-initialze '*s[]' */
  s[1] = sa.s;
  s[2] = NULL;
  getExtOutput(s);
  stralloc_copys(&result,strsplit(result.s,instr(result.s," ") - 1,'L'));
  stralloc_0(&result);
  if (instr(hashast,result.s) == 0) { /* not equal */
    errint(ESOFT,"invalid hashsum of qemud-ast!"); return (-1); }

  char c[] = "setfacl -m g::rw- ";               /* default: set the 'g+w' bit */
  if (str_equal(action->s,"read")) c[15] = '-';  /* change to remove 'g+w' bit */

  /* set acl's of config file 'autostart' */
  if(stat(astfile,&st) == 0) {
    if(!stralloc_copys(&sa,c)) errmem;
    if(!stralloc_cats(&sa,astfile)) errmem;
    if(!stralloc_0(&sa)) errmem;
    pid = fork();
    if (!pid) execlp("bash","bash","-c",sa.s, (char *)NULL);
    wait_pid(&wstat,pid);
  }
  return(0);
}
