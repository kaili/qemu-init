/********************************************************************************
 * liststatus (linkable object file)                                            *
 *                                                                              *
 *  Author: Kai Peter, ©2020-                                                   *
 * License: ISC                                                                 *
 * Version: 0.2                                                                 *
 *                                                                              *
 * Description: List the actual status of all registered machine.               *
 *******************************************************************************/
#define ME ""

#include <unistd.h>
#include <sys/stat.h>
#include "buffer.h"
#include "getln.h"
#include "open.h"
#include "qstrings.h"

#include "convertname.h"
#include "isrunning.h"

char *getDescription(char *n) {
  stralloc sa = {0};
  int fd;
  stralloc fn = {0};
  buffer b;
  char buf[128];
  stralloc line = {0};
  int match;

  if (!stralloc_copys(&sa,"")) errmem;
  if (!stralloc_copys(&fn,path.s)) errmem;
  if (!stralloc_cats(&fn,"/")) errmem;
  if (!stralloc_cats(&fn,lnam.s)) errmem;
  if (!stralloc_cats(&fn,"-qemud.rc")) errmem;
  if (!stralloc_0(&fn)) errmem;

  fd = open_read(fn.s); if (fd == -1) return(sa.s);
  buffer_init(&b,read,fd,buf,sizeof(buf));
  while(1) {
    getln(&b,&line,&match,'\n');
    if (!match) break;
    line.s[byte_rchr(line.s,line.len,'\n')] = '\0'; line.len--;
    if (instr(line.s,"DESC=") == 1) {
      if (!stralloc_copys(&sa,strsplit(line.s,4,QRIGHT))) errmem;
      if (!stralloc_0(&sa)) errmem;
      stralloc_copys(&sa,chrmvs(sa.s,'"'));
      if (!stralloc_0(&sa)) errmem;
      break;
    }
  }
  close(fd);
  return(sa.s);
}

void listStatus(char *regfile) {
  char *true  = "\033[32m is running\033[0m with PID ";    //green
  char *false = " is\033[93m not running\033[0m";          //yellow
  int fd;
  buffer b;
  char buf[128];
  stralloc line = {0};
  int match;
  int pos;
  struct stat st;
  int pid;
  int r;;

  fd = open_read(regfile);
  if (fd == -1) errint(errno,B("Unable to read ",regfile,"!"));
  buffer_init(&b,read,fd,buf,sizeof(buf));
  while(1) {
    getln(&b,&line,&match,'\n');
    if (!match) break;
    line.s[byte_rchr(line.s,line.len,'\n')] = '\0'; line.len--;
    pos = byte_rchr(line.s,line.len,'/');
    if (!stralloc_copys(&path,strsplit(line.s,pos,QLEFT))) errmem;
    if (!stralloc_0(&path)) errmem;
    if (!stralloc_copys(&line,strsplit(line.s,pos,QRIGHT))) errmem;
    if (!stralloc_0(&line)) errmem;

    /* make sure name is not empty and valid */
    if (str_len(line.s) > 1) { r = validateName(&line); } else { r = 1; }

    if ((stat(path.s,&st) == 0) && (r == 0)) {
      convertName(&line);
      buffer_puts(buffer_1,"\033[1m");
      buffer_puts(buffer_1,name.s);
      buffer_puts(buffer_1,"\033[0m");

      char *desc = getDescription(name.s);
      if (str_len(desc) > 1) {
        buffer_puts(buffer_1," (");
        buffer_puts(buffer_1,desc);
        buffer_puts(buffer_1,")");
      }
      pid = IsRunning(&lnam);
      if (pid) {
        buffer_puts(buffer_1,true);
        buffer_puts(buffer_1,fmtnum(pid));
      } else {
        buffer_puts(buffer_1,false);
      }
      buffer_puts(buffer_1,"\n");
      buffer_flush(buffer_1);
    }
  }
  close(fd);
  _exit(0);
}
