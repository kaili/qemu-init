.TH qemud-sio 7 "April 12, 2021" "qemu virtual machine manager"
.SH NAME
qemud-sio \- connect to the qemu monitor
.SH SYNOPSIS
\fBqemud-sio\fR \fI<option> <name> [ command ]\fR
.SH DESCRIPTION
\fBqemud-sio\fR connects to the qemu monitor via unix sockets. Usually it will be invoked
by \fBqemu-adm\fR and/or \fBqemud-init\fR. \fI<name>\fR is the name of a registered virtual
machine, \fIcommand\fR is a qemu monitor command.
.SH OPTIONS
.TP
.I -c|--cmd <command>
Connect to the monitor, execute \fI<command>\fR and exit. \fI<command>\fR is required and
have to be quoted properly.
.TP
.I -m|--mon
Open an interactice connection to the qemu monitor.
.TP
.I -h|--help
Print a short help and exit.
.TP
.I -V|--version
Show versions info and exit.
.SH AUTHOR
Kai Peter, (©2021-present)
.SH SEE ALSO
qemu-adm(1),
qemud-init(8)
