.TH qemud-vnc 8 "April 14, 2021" "qemu virtual machine manager"
.SH NAME
qemu-vnc \- handle vnc connections
.SH SYNOPSIS
\fBqemud-vnc\fR \fI[ options ] | [ <name> ]\fR

.SH DESCRIPTION
\fBqemud-vnc\fR is designed as an executable library to be invoked by \fBqemu-adm\fR.
\fBqemud-vnc\fR does everything on localhost by default. For connetions from external
hosts it is recommended to use ssh with user \fIQUSER\fR. 

Without a valid option (see OPTIONS below) \fBqemu-vnc\fR tries to establish a connection
to \fI<name>\fR. The \fI<name>\fR have to be listed in \fIQVARDIR/vncstatus\fR. \fBqemu-vnc\fR
uses (requires) an external program called \fIvncviewer\fR. The connection is always to
\'localhost:PORT\', whereat the correct PORT of \fI<name>\fR will be read from
\fIQVARDIR/vncstatus\fR automatically.

Without any parameters \fBqemud-vnc\fR prints the (vnc) status of all running virtual
machines from \fIQVARDIR/vncstatus\fR to stdout.

.SH OPTIONS
Options is a series of getopt-style short options. Invalid options will be ignored. Valid
options take precedence overall.
.TP 8
\fI\-w\fR
Iterate through all currently running registered virtual machines to check if vnc is enabled
and (re)write the result to QVARDIR/vncstatus. This option is required by \fBqemud-init\fR.
.TP
\fI\-V\fR
Print version info and exit.

.SH FILES
.SS QVARDIR/vncstatus
\fBqemud-vnc\fR uses \fIQVARDIR/vncstatus\fR to save and read information about registered
virtual machines with vnc enabled.
.SS
<name>-qemud.rc
In \fI<name>-qemud.rc\fR the DISPLAY variable have to be set accordingly to enable vnc through
\fBqemu-init\fR. The \'PORT\' will be calculated automatically based on the port of the tcp
monitor redirection.
.TP
\fIAttention:\fR The here mentioned DISPLAY variable has nothing to do witn \'X\'!

.SH AUTHOR
Kai Peter (©2020-present)
.SH SEE ALSO
qemu-adm(1),qemud-init(8),qemu-init(5)
