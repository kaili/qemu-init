.TH qemud-tap 8 "April 27, 2022" "qemu virtual machine manager"
.SH NAME
\fBqemud-tap\fR \- helper to create a tap network device

.SH DESCRIPTION
\fBqemu-tap\fR is a helper program to create a tap network device for bridge
devices. This is the preferred method in \fIqemu-init\fR. For each bridge
device there will be a symlink \fBqemud-<bridgedev>\fR, where 'bridgedev' is
the name of the bridge device. Example: if your bridge device is \fIbr0\fR the
symlink is \fBqemud-br0\fR.

Howto create a network bridge is outside the scope of \fIqemu-init\fR. Right now
there will be one bridge device respected automatically only. This will may be a
subject to change in the future. Different bridge devices have to be configured
in \fB<name>-qemu.cfg\fR. Example:
.EX

  -device virtio-net-pci,netdev=net0,mac=DE:AD:BE:EF:06:2F
  -netdev tap,id=net0,script=/home/srvcs/qemud/lib/qemud-\fIbr1\fR
.EE

.SH AUTHOR
Kai Peter, (©2021-present)
.SH SEE ALSO
qemu-init(5), qemud-init(8)
