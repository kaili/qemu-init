.TH qemud-acl 8 "April 23, 2021" "qemu virtual machine manager"
.SH NAME
\fBqemud-acl\fR \- set required acl's for files
.SH SYNOPSIS
\fBqemud-acl\fR
.SH DESCRIPTION
\fBqemud-acl\fR does set acl's for files to make sure that \fBqemu-init\fR can operate
properly. These acl's are related to the \fBqemu-init\fR group \fIQGROUP\fR only. All
acl's will be set for each virtual machine separately on start by \fIqemud-init\fR.


- The unix sockets in \fIQRUNDIR\fR have to be writeable by the \fIQGROUP\fR group.
.BR

- The pidfiles have to be readable by group. \fBqemu\fR's \fB-pidfile\fR option creates
  these files with mode 0700.
.BR

- The error log files in \fIQVARDIR\fR have to be writeable by the \fIQGROUP\fR group.

.RE
To disable the autostart of a virtual machine \fIQETCDIR/autostart\fR have to be writeable
by the \fIQGROUP\fR group.

.SH AUTHOR
Kai Peter, (©2021-present)
.SH SEE ALSO
qemu-init(7), qemud-ast(8)
