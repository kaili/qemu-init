.TH qemud-ast 8 "May 2, 2021" "qemu virtual machine manager"
.SH NAME
qemud-ast \- enable or disable autostart of virtual machines

.SH SYNOPSIS
\fBqemud-ast\fR \fI<name>\fR

.SH DESCRIPTION
\fBqemud-ast\fR is designed as executable library to be invoked by \fBqemu-adm\fR.
\fBqemud-ast\fR changes the autostart option of \fI<name>\fR. If autostart is OFF
it will be set to ON and vice versa. The autostart function will be evaluated by
\fBqemud\fR(8) only.

.SH CONFIG FILES
The config file \fIQETCDIR/autostart\fR contains all virtual machines which have
autostart enabled. For security reasons root privileges are required to write to
\fIQETCDIR/autostart\fR (see below).

.SH SECURITY
To enable autostart root privileges are required. Members of the \fIQGROUP\fR are
allowed to disable autostart only.

.SH SEE ALSO
qemu-adm(1),qemud(8)
