.TH qemud 8 "April 13, 2021" "qemu virtual machine manager"
.SH NAME
qemud \- a wrapper for bulk operations with all registered virtual machines

.SH SYNOPSIS
\fBqemud\fR \fI start | stop | status\fR

.SH DESCRIPTION
\fBqemud\fR reads \fIQETCDIR/regfile\fR and inokes \fBqemu-init\fR always with the given action for
each registered virtual machine.

\fBqemud\fR is designed to be included into the init process of the system. Recommended place to do
this is the local part of the init system. Refer to the documentation of your system for information.
\fBqemud\fR accepts the start/stop commands (of a common sysvinit process).

.SH ACTIONS
The actions \fIstart\fR and \fIstop\fR requires root privileges, because they perform a bulk action. The
\fIstatus\fR could be invoked by all members of the group \fIQGROUP\fR.
.SS start
Start each registered virtual machine listed in \fIQETCDIR/autostart\fR. If \fIQETCDIR/autostart\fR
doesn't exists or the virtual machine is not listed, \fBqemud\fR returns 0 silently and does nothing.
Otherwise \fBqemud\fR invokes \fBqemu-init\fR to start the virtual machine.
.SS stop
Stop _all_ currently running registered virtual machines. This is independant from the autostart
configuration. \fBqemud\fR does shutdown the virtual machines in \fIQETCDIR/registered\fR in
sequential order, one after another. This will may be subject to change to do it in parallel.
.SS status
List the short status line by line of all registered virtual machines in the order of
\fIQETCDIR/registered\fR. \fBqemud\fR calls \fBqemud-reg\fR to do this.

.SH CONFIG FILES
\fBqemud\fR uses \fIQETCDIR/registered\fR and \fIQETCDIR/autostart\fR. For details refer to
qemu-init(5). Both files will be read line sequential always.

.SH AUTHOR
Kai Peter (©2017-present)

.SH SEE ALSO
qemu-adm(1),
qemu-init(5),
qemud-init(8),
qemud-reg(8)
