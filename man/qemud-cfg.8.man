.TH qemud-cfg 8 "May 2, 2021" "qemu virtual machine manager"
.SH NAME
qemud-cfg \- read configurations of virtual machines
.SH SYNOPSIS
\fBqemud-cfg\fR \fI[ options ] name\fR
.SH DESCRIPTION
\fBqemud-cfg\fR is designed as an executable library, commonly invoked by
\fBqemu-adm\fR and \fBqemud-init\fR.
.SH OPTIONS
.TP 8
.I -c
Print control pararameters required by \fBqemud-init\fR on fd 3. The format
is KEY=VALUE. The keys will be set in the environment. \fBqemud-init\fR does
use these variables to start \fI<name>\fR.
.TP 8
.I -f
Print the \fIpidfile\fR of \fI<name>\fR with absolute path on fd 3. The format
is \fIPIDFILE=QRUNDIR/<name>-qemu.pid\fR. \fBqemud-init\fR does use it to set
the \fBqemu\fR option \'-pidfile\'.
.TP 8
.I -p
Print the actual configuration from config files of \fI<name>\fR in a human
friendly format to stdout.
.TP 8
.I -r
Print the actual runtime configuration of \fI<name>\fR in a human friendly format
to stdout. If \fI<name>\fR is not running \fBqemud-cfg\fR does fallback to \fI-p\fR
automatically.
.TP 8
.I -x
Read the config files of \fI<name>\fR and print the \fBqemu\fR command line on fd 4.
Required by \fBqemud-init\fR to start a virtual machine.
.TP 8
.I -v
Make \fBqemud-cfg\fR more verbose. Could be used multiple times. Takes effect with
\fI-p\fR or \fI-r\fR only.

.RE
Only one of the options \fI-p\fR, \fI-r\fR or \fI-x\fR could be used at a time.

.SH CONFIG FILES
\fBqemud-cfg\fR reads the configuration from \fI<name>-qemud.rc\fR (required) and
\fI<name>-qemu.cfg\fR (optional). See man qemu-init(5) for details.

.SH RETURN CODES
\fBqemud-cfg\fR returns 0 on success. Otherwise it returns with an error code and
sets \fIerrno\fR accordingly.

.SH AUTHOR
Kai Peter (©2020-present)

.SH SEE ALSO
qemu-adm(1), qemu-init(5), qemud-init(8)
