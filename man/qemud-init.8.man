.TH qemud-init 8 "May 3, 2021" "qemu virtual machine manager"
.SH NAME
qemud-init \- init functions for virtual machines
.SH SYNOPSIS
\fBqemud-init\fR \fI <name> [ start | stop | restart | config | status ]\fR
.SH TERMS
The term \fBqemud-init\fR is explicitly used for the library in \fBQLIBDIR\fR
here. It is _NOT_ the package \fBqemu-init\fR (w/o the 'd').

.SH DESCRIPTION
\fBqemud-init\fR is designed as executable library to be invoked by \fBqemu-adm\fR.
It provides the core functions to start, stop and show information of virtual
machines.

\fBqemud-init\fR requires that a virtual machine is registered by \fI<name>\fR.
The \fI<name>\fR is the unique identifier, it _MUST_ be lower case unique for all
registered virtual machines. This is guaranteed by using \fBqemu-adm\fR for
registration.

\fBqemud-init\fR requires other libraries of the package to operate properly.

.SH OPTIONS
.TP 10
.I start
Start virtual machine \fIname\fR.
.TP 10
.I stop
Stop virtual machine \fIname\fR. \fBqemud-init\fR uses the \fIqemu monitor\fR command
\'system_powerdown\' to shutdown a virtual machine.
.TP 10
.I restart
Restart virtual machine \fIname\fR. Internal \fBqemud-init\fR calls first \fIstop\fR
and afterwards \fIstart\fR. This means \fIname\fR will be stopped completily and then
it starts with a re-read of the configuration files.
.TP 10
.I config
Show the actual configuration settings from config files \fI<name>-qemud.rc\fR and
\fI<name>-qemu.cfg\fR.
.TP 10
.I status
Show status and runtime settings of \fI<name>\fR. If \fI<name>\fR is not running
\fBqemud-init\fR does fallback to \fIconfig\fR.

.SH AUTHOR
Kai Peter (©2019-present)

.SH SEE ALSO
qemu-adm(1),qemud(8),qemu-init(5),qemu-init(7)
