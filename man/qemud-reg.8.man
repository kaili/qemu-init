.TH qemud-reg 8 "April 21, 2021" "qvmm - the qware qemu virtual machine manager"
.SH NAME
qemud-reg \- register, unregister and list virtual machines.
.SH SYNOPSIS
\fBqemud-reg\fR \fI[ -l | -r [ -p <arg> ] [ -U ] | -s | -u ] <name>\fR
.SH DESCRIPTION
\fBqemud-reg\fR is designed as an executable library to be invoked by \fBqemu-adm\fR.
The option handling is to a large extend fault-tolerant. Without any arguments it
behaves like the option \fI-l\fR was specified.
.SH OPTIONS
Options is a series of getopt-style short options. If \fI<name>\fR is required by
an option it should be the last (positional) argument.
.TP 8
.I -l
Print a table of registered virtual machines. This option doesn't require an argument.
If \fI<name>\fR is given it will be ignored silently.
.TP 8
.I -r <name>
Register a virtual machine \fI<name>\fR. To register a virtual machine a location
(path) is required. This could be done either with the \fI-p\fR option or by
specifying \fI<name>\fR with an absolute path. The latter is recommended and
used by \fBqemu-adm\fR. The option \fI-p\fR take precedence over \fI<name>\fR with
an absolute path.

Internally \fBqemud-reg\fR does some checks to validate \fI<name>\fR. Example: If
\fI<name>\fR  contains a dot, everything after and including the first dot will be
cut off.

.TP 8
.I -s
Print the short actual status of all registered virtual machines. The term 'short'
means here that a registered virtual machine is running or not.
.TP 8
.I -u <name>
Unregister virtual machine \fI<name>\fR. This option alone doesn't remove any files.
The options depends an requires an action to take effect.
\. ##################################################
\.
\. ##################################################
\. \\.TP 8
\. .I -p
\. Define the location of a virtual machine. Together with action \fI-r\fR only.
\. \\.TP 8
\. .I -U
\. Set \fIQUSER\fR as owner of all files which will be created for a virtual machine
\. a registration time. Together with action \fI-r\fR only!
.SH CONFIG FILES
All registered virtual machines will be written to \fIQETCDIR/registered\fR, one
per line. A line contains the \fI<name>\fR with the absolute path. It is highly
recommended to NOT edit this file manually. Invalid entries could lead into undefined
behavior or disfunction.
.SH RETURN CODES
\fBqemud-reg\fR returns 0 on success. Otherwise it returns non zero witth an error code.
.SH AUTHOR
Kai Peter (©2020-present)
.SH SEE ALSO
qemu-adm(1)
