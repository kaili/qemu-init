.TH "qemu manager" 5 "October 9, 2021" "qemu virtual machine manager"
.SH NAME
qemu-init configuration, status and other files.

.SH TERMS
The term \fBqemu-init\fR is the name of the package. It is NOT related to the library
file \fIQLIBDIR/qemud-init\fR.

The term \fI<name>\fR is the unique identifier used to determine all files which belongs to one
dedicated virtaul machine called \fIname\fR.

.SH DESCRIPTION
\fBqemu-init\fR uses several files which could be classified. These files are described below:

.SH CONFIG FILES
\fBqemu-init\fR uses several config files. Files in \fIQETCDIR\fR are global for \fIqemu-init\fR.
The files \fI<name>-qemud.rc\fR and \fI<name>-qemu.cfg\fR are specific for the related virtual
machine \fI<name>\fR only.
.SS
.I QETCDIR/registered
This file contains all virtual machines registered by \fBqemu-adm\fR including the absolute
path. Path is the location where all files which belongs to a registered virtual machine of
\fI<name>\fR have to be located.
.SS
.I QETCDIR/autostart
This file contains all virtual machines which will be started automatically by \fBqemud\fR.
.SS
.I QETCDIR/vncoffset
This file contains an offset for the first vnc port. The offset will be increased by 1 for each
virtual machine with vnc enabled through \fBqemu-init\fR. The number have to be in the first line.
If the file doesn't exist the internal offset 50000 will be used.
.SS
.I <name>-qemud.rc
This file contains the default parameters of \fBqemu-init\fR. It will be created at registration
time. It _MUST_ exist for each \fI<name>\fR. The format is KEY=VALUE. All after an '#' will be
treated as comment. Most of the entries will be converted and passed as option to \fBqemu\fR.
Some entries does control the behavior of the virtual machine \fI<name>\fR, used by \fBqemud-init\fR
only:
.TP
.I BOOTDELAY
At boot time of the host it could be required to wait some time before a virtual machine \fI<name>\fR
could be started w/o an error. The \fIBOOTDELAY\fR option delays the start of the virtual machine
by \fIn\fR seconds. The start process of \fI<name>\fR will be put into the background.
.TP
.I STOPDELAY
The timeout in seconds \fBqemu-init\fR will wait that the virtual machine does shutdown (default: 40).
Some times a clean shutdown can take longer. Increasing \fISTOPDELAY\fR prevents a false positive
error. Values smaller than the default will be ignored.
.TP
.I PRECMD,POSTCMD
Some times it is required to run one or multiple commands immediately before a virtual machine
starts (\fIPRECMD\fR) and/or after (\fIPOSTCMD\fR) a virtual machine was started. These options
could be used to define a single command or batch file to execute the necessary commands. Maybe
an absolute path is required as well appropriate access rights.

A value will be executed with the 'exec' command of the shell (default: bash). Thus, batch files
must not have the executable bit set. If some thing went wrong \fBqemud-init\fR continues and may
or may not print an error message.
.SS
.I <name>-qemu.cfg
This file can contain additional \fBqemu\fR options not covered by \fI<name>-qemu.cfg\fR.
It will be created at registration time. It must not exist for each \fI<name>\fR
and/or it doesn't have to contain valid entries. The format is flexible to a great
possible extent. However, all after a '#' will be treated as comment. Entries
will be converted and passed as option to \fBqemu\fR.
.TP
.I qemu option style
The file contains \fBqemu\fR command line options only. The file shall be work with the
'-readfile' option.
.TP
.I qemu shell script style
The file is a qemu start script. It shall start a virtual machine with 'sh </path/to/><name>-qemu.cfg'.
Shell variables could be used as well trailing backslashes at the end of lines.

.RE
Some variations/combinations of the two styles are possible. This could break the usage at the
command line outside of \fBqemu-init\fR.

.SH STATUS FILES
.SS
.I QVARDIR/vncstatus
This file contains information of all running virtual machines which have \fIDISPLAY="vnc"\fR
set in \fI<name>-qemud.rc\fR. It is a colon delimited v3 file. Fields:

	<name>:host:vncport

\fIhost\fR is the fqdn of the \fBqemu\fR host. \fIvncport\fR is the port where the internal
\fBqemu\fR vnc server is listening. The \fIvncport\fR will be calculated based on the tcp port
for monitor redirection.

.SH TEMPLATE FILES
.SS
.I QETCDIR/template-qemud.rc
This file will be used to create \fI<name>-qemud.rc\fR by \fBqemu-adm\fR at registration time.
Some default values will be set automatically.

.SS
.I QETCDIR/template-qemu.cfg
This file will be used to create \fI<name>-qemu.cfg\fR by \fBqemu-adm\fR at registration time.
By default it contains commented lines only.

.SH OTHER FILES
.SS
.I sudo-qemud
By design \fBqemu-init\fR requires some root privileges for operation. These privileges are
granted here. Usually it will be installed in the 'sudoers.d' directory.
.SS
.I QVARDIR/<name>-error.log
Log errors on start up of a virtual machine \fI<name>\fR. Doesn't exists if no error occurs.

.SH AUTHOR
Kai Peter (©2017-present)
.SH SEE ALSO
qemu-init(7),qemu-adm(1),qemud-init(8),qemud(8)
