.TH qemu-adm 1 "April 4, 2021" "qemu virtual machine manager"
.SH NAME
qemu-adm \- the \fBqemu-init\fR administration tool
.SH SYNOPSIS
.B qemu-adm\fR \fI[ -a|-r|-u ] <name>\fR
.br
.B qemu-adm\fR \fI[ -S|-D|-R|-m|-s|--config|--vnc ] <name>\fR
.br
.B qemu-adm\fR \fI-c <command> <name>\fR
.br
.B qemu-adm\fR \fI[ -l ] | [ -h ] | [ -V ]\fR

.SH DESCRIPTION
\fBqemu-adm\fR is the main tool of \fBqemu-init\fR. It provides the complete functionality of
\fBqemu-init\fR through a unique interface to the library functions.

.SH TERMS
Except explicit otherwise stated the term \fI<name>\fR is the short unique identifier which belongs to
one dedicated virtual machine. It have to be unique between all registered virtual machines handled
by \fB\qemu-adm\fR. \fI<name>\fR is be used as prefix for several files as well for the \fBqemu\fR
option \fI"\-name"\fR and have to consist of alphanumeric characters only. Especially a '-'  (minus)
and a '.' (dot) are not allowed.

.SH OPTIONS
There are two types of parameters: \fBactions\fR and \fBoptions\fR. Only one \fBaction\fR is
allowed at execution time.

If multiple \fBactions\fR are given the first one wins - all other will be ignored and a warning
will be printed. Maybe the behavior of \fBqemu-adm\fR will be undefined in such a case.

\fBoptions\fR extends \fBactions\fR with additional functionality.
.SS \fBActions:\fR
.TP
.I \-a | --autostart <name>
Enable or disable autostart of the virtual machine \fI<name>\fR. If the actual value is ON then
it will be set to OFF and vice versa. All virtual machines with autostart enabled are listed in
\fIQETCDIR/autostart\fR.

.TP
.I \-c | --cmd <command> <name>
Execute \fI<command>\fR in the qemu monitor of \fI<name>\fR and exit. \fI<command>\fR have
to be quoted properly. This action requires 2 arguments; an invalid number of arguments will
result in an error.
.TP
.I \--config <name>
Show configuration of \fI<name>\fR from its config files \fI<name>-qemu.rc\fR and \fI<name>-user.cfg\fR.
.TP
.I \-D | --stop <name>
Stop virtual machine \fI<name>\fR.
.TP
.I \-h | --help
Show the help screen and exit.
.TP
.I \-l | --list
List all registered virtual machines from \fIQETCDIR/registered\fR.
.TP
.I \-m | --monitor <name>
Connect to the qemu monitor of \fI<name>\fR for interactive usage.
.TP
.I \-r | --register <name>
A virtual machine given by \fI<name>\fR will be created and registered. \fI<name>\fR MUST contain an
absolute path here: \fI/full/path/to/name\fR. The path will be separated from \fI<name>\fR and defines
the location for all files which belongs to this dedicated virtual machine. If the path doesn't exist it will be created. Inside
this directory the files \fI<name>-qemu.rc\fR and \fI<name>-user.cfg\fR will be created. If the \fI\-f\fR
option is given a default disk image file will be created.
.br
All file names begins with prefix \fI<name>\fR (without the path) separated by a '-' (dash).
Existing files will not be overwritten.
.TP
.I \--reset <name>
Perform a \(dqwarm start\(dq of \fI<name>\fR. This will be done with the qemu monitor command \fIsystem_reset\fR.
.TP
.I \-R | --restart <name>
Restart the virtual machine \fI<name>\fR. This executes the start/stop functions
of \fBqemud-init\fR. The virtual machine will be stopped and afterwards new started.
.TP
.I \-s | --status <name>
Show the actual runtime configuration of \fI<name>\fR. If \fI<name>\fR is not given
it shows a short status of all registered virtual machines,
.TP
.I \-S | --start <name>
Start the virtual machine \fI<name>\fR.
.TP
.I \-u | --unregister <name>
Unregister the virtual machine \fI<name>\fR. The entry will be removed from \fIQETCDIR/registered\fR.
All existing files of virtual machine \fI<name>\fR stay untouched.
.TP
.I \--vnc <name>
Calls \fBqemud-vnc\fR to start a vncviewer for \fI<name>\fR. If \fI<name>\fR is not
specified the actual vnc status will be shown.  Refer to qemud-vnc.8 for details of
its behavior.
.TP
.I \-V | --version
Show version info and exit.
.SS \fBOptions:\fR
.TP
.I \--cdrom [ <file> ]
Mount \fI<file>\fR at the first virtual cdrom drive. Usually \fI<file>\fR is an iso image. If
\fI<file>\fR is not given, the first cdrom drive will be ejected. This option works with
the action \fIreset\fR only.

\. .SH FILES

.SH EXAMPLES
.EX
Register a new virtual machine with name 'pluto' and create all files:
    $> qemu-adm -r /home/vm/pluto -f
.EE

.EX
Unregister a virtual machine with name 'pluto':
    $> qemu-adm -u pluto
.EE

.EX
Check the char devices of 'pluto' through the qemu monitor:
    $> qemu-adm -c "info chardev" pluto
.EE

.SH AUTHOR
Kai Peter (©2017-present)
.SH SEE ALSO
\. qemu-adm(5),qemu-adm(7),
qemud-init(8),qemud(8)
