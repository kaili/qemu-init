/********************************************************************************
 * isrunning (linkable object file)                                             *
 *                                                                              *
 *  Author: Kai Peter, ©2020-present                                            *
 * License: ISC                                                                 *
 * Version: 0.2                                                                 *
 *                                                                              *
 * Description: Get output of an external program in 'result'.                  *
 *******************************************************************************/
#include <unistd.h>
#include "buffer.h"
#include "errmsg.h"
#include "fd.h"
#include "qstrings.h"
#include "wait.h"

stralloc result = {0};

int getExtOutput(char **c) {
  int pi[2];
  int wstat;
  buffer b;
  char buf[128];
  char ch;
  stralloc sa = {0};

  if (pipe(pi) == -1) errsys(errno);

  int pid = fork();
  switch (pid) {
  case -1: errsys(errno);
  case 0:  /* forked child */
    if(fd_move(1,pi[1]) == -1) errsys(errno);
    close(pi[0]); close(pi[1]);
    execvp(*c,c);
    errint(errno,"Unable to fork");
  }
  if(fd_move(0,pi[0]) == -1) errsys(errno);
  close(pi[0]); close(pi[1]);

  buffer_init(&b,read,0,buf,sizeof(buf));
  while (buffer_get(&b,&ch,1) == 1) {
    if(!stralloc_catb(&sa,&ch,1)) errmem;
  }
  if(!stralloc_copy(&result,&sa)) errmem;
  wait_pid(&wstat,pid);
  return 0;
}
