/********************************************************************************
 * qemud-reg (executable library)                                               *
 *                                                                              *
 *  Author: Kai Peter, ©2020-                                                   *
 * License: ISC                                                                 *
 * Version: 0.6                                                                 *
 *                                                                              *
 * Description: Provide actions around the registration of virtual machines. It *
 *              does register, unregister and list them.                        *
 *******************************************************************************/
#define ME "qemud-reg"
#define VERSION "0.7"

#include <unistd.h>
#include <sys/stat.h>
#include "buffer.h"
#include "case.h"
#include "getln.h"
#include "getoptb.h"
#include "open.h"
#include "qstrings.h"
#include "wait.h"

#include "autostart.h"
#include "convertname.h"
#include "getextoutput.h"
#include "register.h"

#include "layout.h"

/* option flags */
unsigned int flag_user = 0;

int doList(void) {
  char buf[128];
  buffer b;
  int fd;
  char *fn = regfile;
  stralloc line = {0};
  int match;
  int pos;
  int i;
  int pid;
  char *ast;

  if (!name.len) {       /* no 'name' was spezified - list all registered vm's */
    fd = open_read(fn);
    if (fd < 0) errint(errno,B("Couldn't open ",fn,"!"));
    buffer_init(&b,read,fd,buf,sizeof(buf));
    buffer_puts(buffer_1,"\033[1mRegisterd virtual machines:\033[0m\n");

    buffer_puts(buffer_1,"Name                |Location                                |Autostart\n");
    buffer_puts(buffer_1,"--------------------+----------------------------------------+---------\n");
    buffer_flush(buffer_1);

    while (1) {
      getln(&b,&line,&match,'\n');
      if (!match) break; /* EOF */
      /* ignore empty lines and "invalid" entries (e.g. lines with '#') */
      if ((line.len > 0) && (line.s[0] == '/')) {
        pos = byte_rchr(line.s,str_len(line.s),'\n');    /* remove '\n' */
        if (!stralloc_copys(&line,strsplit(line.s,pos,QLEFT))) errmem;
        if (!stralloc_0(&line)) errmem;
        stralloc_init(&path);       /* path must not have a lenght, ...  */
        convertName(&line);         /* otherwise it will not be set here */
        buffer_puts(buffer_1,name.s);
        for (i=name.len;i<22;i++) buffer_puts(buffer_1," ");   /* filler */
        buffer_puts(buffer_1,path.s);
        for (i=i + path.len;i<63;i++) buffer_puts(buffer_1," ");   /* filler */
        ast = (hasAutostart(lnam.s) == 1?" ON":" OFF");
        buffer_puts(buffer_1,ast);
        buffer_puts(buffer_1,"\n");
        buffer_flush(buffer_1);
      }
    }
    close(fd);
    _exit(0);
  }

  /* list config file parameters of vm 'name' */
  if (chdir(libdir) == -1) errint(errno,libdir);

  int wstat;
  char *(c[4]);
  c[0] = "./qemud-cfg";
  c[1] = "-p";
  c[2] = name.s;
  c[3] = 0;
  pid = fork();
  if (!pid) { execvp(*c,c); if (errno) errint(errno,"qemud-cfg"); }
  wait_pid(&wstat,pid);

  return(0);
}

int main(int argc, char **argv) {
  struct stat st;
  int opt;
  char *action = NULL;
  stralloc parg = {0};    /* positional argument (required, except for "list") */

  /* Before we do anything, the 'regfile' _MUST_ exist (even if it's empty)!  The
     'regfile' is owned by root and the parent folder have to have the sticky bit
     set - thus only root is able to delete the 'regfile'.                     */
  if (stat(regfile,&st) != 0) errint(errno,B("Unable to stat ",regfile,"!"));

  while ((opt = getopt(argc,argv,"lp:rsuUV")) != opteof)
  switch(opt) {
    case 'l': action = "l"; break;
    case 'r': action = "r"; break;
    case 's': action = "s"; break;
    case 'u': action = "u"; break;
    case 'p': if(!stralloc_copys(&path,optarg)) errmem; break;
    case 'U': flag_user = 1; break;    /* with user id 0 (root) only !!! */
    case 'V': out(B(ME,"-",VERSION)); _exit(0); break;
    default: action = "l"; break;
  }
  argc -= optind;
  argv += optind;

  if (!action) action="l";  /* fallback */
  if (*action == 's') listStatus(regfile);
  if (!*argv) {
    if (*action == 'l') doList();    /* special: w/o an argument list all vm's */
    errint(EINVAL,"Missing argument 'name'!");
  }
  /* save the positional '+argv' in 'parg' */
  if(!stralloc_copys(&parg,*argv)) errmem;
  stralloc_0(&parg);

  convertName(&parg);
  stralloc_free(&parg);

  if (*action == 'l') doList();
  if (*action == 'r') doRegister();
  if (*action == 'u') doUnregister();

  _exit(0);
}
