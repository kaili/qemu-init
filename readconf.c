/********************************************************************************
 * readconf (executable file)                                                   *
 *                                                                              *
 *  Author: Kai Peter, ©2020-                                                   *
 * License: ISC                                                                 *
 * Version: 0.4                                                                 *
 *                                                                              *
 * Description: Read the <name>-qemud.rc/<name>-qemu.cfg file(s).               *
 *******************************************************************************/
#define ME "readconf"

#include <unistd.h>
#include <sys/stat.h>
#include "case.h"
#include "errmsg.h"
#include "getln.h"
#include "open.h"
#include "qstrings.h"

#include "convertname.h"
#include "readconf.h"

void usroptsInvalid(char *e) {
  errint(EINVAL,B(e," is not allowed in ",lnam.s,"-qemu.cfg!"));
}

stralloc parseLine(stralloc *line) {
  /* these steps are required for each config line */
  stralloc sa = {0};
  int pos;

  stralloc_copys(&sa,line->s);
  /* first remove '\n' */
  pos = instr(sa.s,"\n");
  if(!stralloc_copys(&sa,strsplit(sa.s,pos-1,'L'))) errmem;
  stralloc_0(&sa);
  /* second remove all leading spaces */
  stralloc_copys(&sa,ltrim(sa.s));
  stralloc_0(&sa);
  /* third replace all tabs '\t' by a single space */
  stralloc_copys(&sa,chrpls(sa.s,'\t',' '));
  stralloc_0(&sa);
  /* fourth remove comments (everything after a '#') */
  pos = instr(sa.s,"#");
  if(pos > 0) {
    if(!stralloc_copys(&sa,strsplit(sa.s,pos-1,'L'))) errmem;
    stralloc_0(&sa);
  }
  return(sa);
}

int getUserOpts(stralloc *fn) {
  int fd;
  buffer b;
  char buf[128];
  stralloc line = {0};
  int match;
  int pos;

  if (!stralloc_copys(&usropts,"")) errmem;  /* XXX: important! */
  fd = open_read(fn->s);
  if (fd < 0) errint(errno,B("Unable to read ",fn->s,"!"));
  buffer_init(&b,read,fd,buf,sizeof(buf));

  while (1) {
BeginReadLine:
    if (getln(&b,&line,&match,'\n') == -1) return -1;
    if (!match) break;
    stralloc_copys(&line,parseLine(&line).s);
    stralloc_0(&line);

    /* an empty line or the whole line is a comment --> next line ... */
    if(line.len <= 1) goto BeginReadLine;
    /* ... otherwise it seems the line contains data ;-) --> go on */
    if (!stralloc_0(&line)) errmem;

    /* remove all trailing spaces */
    stralloc_copys(&line,rtrim(line.s));
    if (!stralloc_0(&line)) errmem;

    /* remove all (surrounding) double quotes */
    stralloc_copys(&line,chrmvs(line.s,'"'));
    if (!stralloc_0(&line)) errmem;

    /* remove trailing backslash */
    if (instr(line.s,"\\")) {
      stralloc_copys(&line,str_rtrim(line.s,(char)92));
      if (!stralloc_0(&line)) errmem;
    /* Hint: removing trailing spaces is not required again! */
    }

    /* remove shell style variables */
    while ((instr(line.s,"$")) > 0) {
      pos = byte_chr(line.s,line.len,'$');
      for(;;) {
        if(line.s[pos] == ' ') break;
        line.s[pos] = ' '; pos++;
      }
    }

    /* split KEY=VALUE pairs */
    if ((byte_chr(line.s,line.len,'=')) < (byte_chr(line.s,line.len,'-'))) {
      pos = byte_chr(line.s,line.len,'=');
      if(!stralloc_copys(&line,strsplit(line.s,pos,'R'))) errmem;
      if(!stralloc_0(&line)) errmem;
    }

    /* ignore shell environment variables */
    if (instr(ltrim(line.s),"-") != 1) goto BeginReadLine;

    /* now add the line to usropts */
    if (!stralloc_cats(&usropts," ")) errmem;
    if (!stralloc_cats(&usropts,line.s)) errmem;
  }
  close(fd);
  if (!stralloc_0(&usropts)) errmem;
  if(str_len(usropts.s) == 0) return(0);  /* no user options (found) */

  /* convention: the qemu option '-name' is not allowed in usropts */
  if (instr(usropts.s,"-name ") > 1) usroptsInvalid("The option '-name'");

  /* remove 'qemu-system-*' from usropts */
  if ((pos = (instr(usropts.s,"qemu-system-")) - 1))
    for (;;) {
      if (usropts.s[pos] == ' ') break;
      usropts.s[pos] = ' '; pos++;
    }

  /* remove sequently spaces */
  for (pos=0;pos<=usropts.len;pos++) {
    if(usropts.s[pos] == ' ')
      if(usropts.s[pos+1] == ' ') {
        usropts.s[pos] = 126;    /* tilde */
      }
  }
  if(instr(usropts.s,"~")) {
    stralloc_copys(&usropts,chrmvs(usropts.s,'~'));
    stralloc_0(&usropts);
  }
  return 0;
}

int readConfig(void) {
  int fd;
  buffer b;
  char buf[512];
  stralloc fn = {0};
  stralloc line = {0};
  int match;
  int pos;
  char *key = NULL;
  char *value = NULL;
  struct stat st;

  /* First we read the <name>-qemu.cfg file (qemu style options format). Later on
   * the options will be compared with the 'standard default' one's: every option
   * which was defined by the user overwrites the default one's completely!    */
  if (!stralloc_copys(&fn,path.s)) errmem;
  if (!stralloc_cats(&fn,"/")) errmem;
  if (!stralloc_cats(&fn,lnam.s)) errmem;
  if (!stralloc_cats(&fn,"-qemu.cfg")) errmem;
  if (!stralloc_0(&fn)) errmem;
  if (!stralloc_copys(&usropts,"")) errmem;    /* XXX: initialize usropts now! */
  if (!stralloc_0(&usropts)) errmem;
  if (stat(fn.s,&st) == 0)
    if (getUserOpts(&fn) == -1) return (-1);  // better: exit with error ???

  /* Second we read the <name>-qemud.rc file */
  stralloc_init(&fn);
  if (!stralloc_copys(&fn,path.s)) errmem;
  if (!stralloc_cats(&fn,"/")) errmem;
  if (!stralloc_cats(&fn,lnam.s)) errmem;
  if (!stralloc_cats(&fn,"-qemud.rc")) errmem;
  if (!stralloc_0(&fn)) errmem;

  if (!stralloc_copys(&stdopts,"")) errmem;
  fd = open_read(fn.s);
  if (fd < 0) errint(errno,B("Unable to read ",fn.s,"!"));
  buffer_init(&b,read,fd,buf,sizeof(buf));

  while(1) {
    key = ""; value = "";
    if (getln(&b,&line,&match,'\n') == -1) return (-1);
    if (!match) break;
    stralloc_copys(&line,parseLine(&line).s);
    stralloc_0(&line);

    /* ignore empty lines (usually were comments before) */
    if (!strequal(line.s,"")) {
      pos = byte_chr(line.s,line.len,'=');
      key = strsplit(line.s,pos,'L');
      key=trim(key);
      value = strsplit(line.s,pos,'R');
      /* remove all (surrounding) double quotes and spaces from value */
      value = chrmvs(value,'"');
      value = trim(value);
      getControls(key,value);
      getPeripherals(key,value);
      setStdOpts(key,value);
    }
  }    /* end 'while' (read) */
  close(fd);
  return(0);
}
