#********************************************************************************
# qemud-init - start/stop qemu virtual machines                                 *
#                                                                               *
#  Author: Kai Peter, ©2016-present                                             *
# Version: 0.105.4                                                              *
# License: ISC                                                                  *
#********************************************************************************
VERSION=`cat $0 | grep -m 1 'Version' | cut -d\   -f3`       # get version number
ME=`basename $0`                                             # set program name

PATH=`dirname $0`:$PATH
declare -i DBG=0

usage() { echo "Usage: "$ME" <name> start|stop|restart|status|config"; }

error() {
  ERRMSG1="Seems another instance of $FULLNAME is running already!"
  ERRMSG2="Couldn't start $NAME: `[ -f $ERRLOG ] && cat $ERRLOG`"
  ERRMSG3="$FULLNAME is not running!"
  printf "\033[1m  error: $(eval echo \"\$$(echo ERRMSG$1\"))\033[0m\n"
  exit 111
}
#********************************************************************************
result() {
  C=`tput cols` ; C=$(($C-4))   # calculate cursor position (column)
  printf "\033[${C}G"           # move cursor to column ${C} in actual line
  [ "$EC"  = "B" ] && printf "\033[1m\033[93m%s\033[0m\n" "--" && return # BOOTDELAY
  [ "$EC"  = "0" ] && printf "\033[32m%s\033[0m\n" "OK"
  [ "$EC" != "0" ] && printf "\033[1m\033[91m%s\033[0m\n" "!!"
}

check() {                        # return code: 0 if VM is running or 1 otherwise
  [ -f "$PIDFILE" ] && pgrep -F "$PIDFILE" &>DEVNULL ;
  EC=$? ; return "$EC"
}

execCmd() {   # execute command/script
  local CMD=$@

  CMD=`echo $CMD | cut -d# -f1`    # ignore comments
  [ -z "$CMD" ] && return
  if [ -f "$CMD" ] ; then sh ${CMD}; return; fi
  exec ${CMD} &
  return
}

config() { qemud-cfg -p $NAME; }     # display the configuration from config file

start() {
  local CMDLINE

  echo -n "Starting $FULLNAME ..."
  check ; [ "$EC" -eq 0 ] && EC=1 && result && error 1          # already running

  CMDLINE=`qemud-cfg -x $NAME 4>&1` || exit $?
  if [ -z "$CMDLINE" ] ; then
    echo "Unable to get qemu parameters! Exiting!" ; exit 111 ; fi
#echo $CMDLINE
  OPTS=`echo $CMDLINE | cut -d\  -f4-`

  #******************************************************************************
  # The BOOTDELAY option will be used with qemud only.  It have to be an integer,
  # the value have to be greater than 0. Invalid values will be converted to 0.
  [ "$QEMUINIT" ] && declare -i INT="$BOOTDELAY"        # check for integer value
  if [ "$INT" ] && [ "$INT" -gt 0 ] ; then
    # remove $QEMUINIT from environment and  start a new instance in background -
    # ... and yes, it works perfect! And it is very simple, isn't it? :)
    unset QEMUINIT ; ( sleep "$INT" && "$ME" "$VMRC" start >DEVNULL & )
    EC="B" && echo -n " ($INTs delayed)" && result        # if it fails - bummer!
    exit 0
  fi

  PRECMD=$(grep "^PRECMD" $RCFN | cut -d= -f2- | sed 's/"//g'); execCmd "$PRECMD"
  sudo "$QEMUD_TARGET" -name "$FULLNAME" $OPTS &>$ERRLOG
  # immediately set the acl's (before 'check')
  # with sudo the full absolute path is required for 'qemud-acl'
  [ -f "$PIDFILE" ] && sudo QLIBDIR/qemud-acl -s -p "$PIDFILE" "$NAME"
  check ; result "$EC" ; [ $EC -ne 0 ] && error 2        # $EC comes from check()
  [ ! -s "$ERRLOG" ] && rm "$ERRLOG"

  POSTCMD=`grep ^POSTCMD $RCFN | cut -d= -f2- | sed 's/"//g'`; execCmd "$POSTCMD"
  qemud-vnc -w || exit $?
}

down() {
  # return silently if not running _and_ QEMUINIT is set (called by 'qemud') ...
  check ; [ "$EC" -ne 0 ] && [ "$QEMUINIT" ] && return
  # ... otherwise ...
  echo -n "Shutting down $FULLNAME ..."
  # ... print an error message (called by 'qemu-adm')
  [ "$EC" -ne 0 ] && result && error 3

  qemud-sio --cmd "system_powerdown" $NAME &>DEVNULL
  if [ "$EC" -ne 0 ] ; then result 0 ; exit "$EC" ; fi

  [ "$STOPDELAY" ] && declare -i INT="$STOPDELAY"
  [ "$INT" ] && [ "$INT" -gt 9 ] && STOPDELAY="$INT" || STOPDELAY=40
  i=1   # check if VM is down (default: timeout 40 seconds)
  until [ $i -gt "$STOPDELAY" ] ; do
    check ; [ "$EC" -ne 0 ] && break
    echo -n "." ; sleep 1
    i=$(($i+1))
  done
  # we have to reverse the return codes here
  [ "$EC" = 0 ] && EC=1 || EC=0 ; result $EC
  qemud-vnc -w || exit $?
}

restart() { down && start; }

status() {
  qemud-cfg -r $NAME      # '-r' will be changed to '-p' if vm is not running
}


case $1 in           # ----- the basic options every program should have --------
  -h|--help)    usage ; exit 0;;
  -V|--version) echo "$ME-$VERSION" ; exit 0;;
esac

# Special case: w/o NAME list run status of all VM's ('qemu-adm -s' w/o <name>)
# Design convention: use 'qemud status' to list status for all VM's always - even
#                    if calling directly 'qemud-reg -s' would be more efficient.
if [ $1 = "status" ] ; then QBINDIR/qemud status ; exit $? ; fi
NAME=$1

case $2 in
    start) ACTION="start";;
     stop) ACTION="down";;
  restart) ACTION="restart";;
   status) ACTION="status";;
   config) ACTION="config";;
        *) usage; exit 111;;
esac
export `qemud-cfg -c -f $NAME 3>&1` &>DEVNULL

#********************************************************************************
RCFN="$QEMUD_PATH/$QEMUD_NAME-qemud.rc" ; [ -f "$RCFN" ] || exit 111
DESC=`grep ^DESC $RCFN | sed 's/"//g' | cut -d= -f2-`
[ "$DESC" ] && FULLNAME="$NAME ($DESC)" || FULLNAME="$NAME"

ERRLOG=QVARDIR/$QEMUD_NAME-error.log

${ACTION}                 # now run the requested action
exit $?
