/********************************************************************************
 * checkast (linkable object file)                                              *
 *                                                                              *
 *  Author: Kai Peter, ©2020-                                                   *
 * License: ISC                                                                 *
 * Version: 0.4                                                                 *
 *                                                                              *
 * Description: Check if 'autostart' is enabled for 'name'. Return codes:       *
 *              -1: read error                                                  *
 *               0: false, autostart is not enabled                             *
 *               1: true, autostart is enabled                                  *
 *               2: invalid entry for 'name'                                    *
 *******************************************************************************/
#define ME ""

#include "case.h"
#include "errmsg.h"
#include "qstrings.h"
#include "readclose.h"

#include "autostart.h"
#include "convertname.h"

stralloc as = {0};   /* XXX: _NOT_ 'sa' */

int hasAutostart(char *s) {
  char *x;
  int i;

  if (as.len == 0)
    if (!openreadclose(astfile,&as,64)) return(-1);    /* return on read error */
  case_lowers(as.s);          /* who knows what the user did: invalid entries? */
  strcpys(&x,s);
  strcats(&x,"\n");           /* the line in 'as' must end with seperator '\n' */
  i = instr(as.s,x);
  if (i == 1) return(1);      /* make sure the line starts with 'lnam' always  */
  /* beware of the offset: i is as.s[i-1], thus we need as.s[i-2] */
  if ((i > 1) && (as.s[i-2] == 0x0A)) return(1);
  /* The 'name' was found in autostart, but something is wrong.  Perhaps the file
   * 'autostart' was edited manually?  Anyway, the caller function have to handle
   * this invalid entry.                                                       */
  if (i > 1) return 2;
  return(0);
}
