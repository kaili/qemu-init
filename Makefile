# Makefile qemu-vmm

GNUMAKEFLAGS += --no-print-directory

SHELL ?= sh
PACKAGE = `head -1 PKGNAME.dist`
DESTDIR = $(PACKAGE)

TARGETS = qemud qemu-adm qemud-init
TARGETS+= qemud-acl qemud-ast qemud-cfg
TARGETS+= qemud-reg qemud-sio qemud-tap qemud-vnc
TARGETS+= vncconnect
TARGETS+= dot-ssh-qemud sudo-qemud

CCFLAGS = -I./qlibs/include -Iqlibs
LDFLAGS = -Lqlibs
COMPILE = ./compile $(CCFLAGS)
LOADBIN = ./load
QLIBS = $(LDFLAGS) -lqlibs

PKGDIR = `grep '^PKGDIR' ac-layout | cut -d\  -f2`
HOME   = $(PKGDIR)
PREFIX = `head -1 ac-prefix`
BINDIR = `grep '^BINDIR' ac-layout | cut -d\  -f2`
ETCDIR = `grep '^ETCDIR' ac-layout | cut -d\  -f2`
LIBDIR = `grep '^LIBDIR' ac-layout | cut -d\  -f2`
VARDIR = `grep '^VARDIR' ac-layout | cut -d\  -f2`
DATDIR = `grep '^DATDIR' ac-layout | cut -d\  -f2`
RUNDIR = `grep '^RUNDIR' ac-layout | cut -d\  -f2`

SED = `head -1 ac-sed`

#USER  = `grep -m 1 ^USR ac-usrgrp | awk '{ print $$2; }' | cut -d: -f1`
#GROUP = `grep -m 1 ^GRP ac-usrgrp | awk '{ print $$2; }' | cut -d: -f1`
USER  = `head -2 ac-usrgrp | tail -1`
GROUP = `head -1 ac-usrgrp`
.PHONY: install $(TARGETS)

default all: prepare libs $(TARGETS) man-pages
	@echo `date "+%Y%m%d %H%M"` > .build

# recipe 'chkbash' uses the variable 'BASHERR' to be compact in itself
BASHERR = "\033[91mnot found!\033[0m\\n\033[1m'bash' is required"
BASHERR+= "to build and install as well to run "$(PACKAGE)"!\033[0m"
chkbash:
	@echo -n "Checking for bash (required) ... "
	@bash -c "echo OK!" 2>config.log || ( echo -e $(BASHERR) ; exit 2 )

distclean: clean
	@rm -rf $(PACKAGE)
	@rm -f *.log
	@cd qlibs && $(MAKE) clean

clean:
	@echo -n Cleaning up files ...
	@rm -f *.o *.a $(TARGETS) qemud-br*
	@rm -f .build compile load install systype ac-*
	@rm -rf *.tmp install.log
	@echo " done!"
	@cd man ; $(MAKE) clean

conf-stat:
	@[ -f compile ] && [ -f $(LOADBIN) ] && [ -f ac-layout ] || ./configure auto
#	echo BINDIST: $(QUBE_BUILD)

build-stat: conf-stat
	@[ -f install ] || $(MAKE) default

MAKEERR="\\033[91mIncompatible 'make' tool, GNU make \(gmake\) is required!\\033[0m"
prepare: conf-stat
	@$(MAKE) --version | grep -q 'GNU [Mm]ake' || ( echo -e "$(MAKEERR)" ; exit 3 )
	@cat ac-warn.sh ac-insthdr.tpl install.sh \
	| $(SED) -e '/###manInstall###/{r ac-instman.tpl' -e 'd}' \
	| $(SED) -e '/###symInstall###/{r ac-instlnk.tpl' -e 'd}' \
	| $(SED) -e '/^mkUsrGrp()/{r ac-mkusrgrp.tpl' -e 'd}' \
	| $(SED) /NIL=/s}QDEVNULL}"`head -1 ac-devnull`"}g \
	| sed s}QPREFIX}"$(PREFIX)"}g \
	| sed s}QBINDIR}"$(BINDIR)"}g \
	| sed s}QETCDIR}"$(ETCDIR)"}g \
	| sed s}QLIBDIR}"$(LIBDIR)"}g \
	| sed s}QVARDIR}"$(VARDIR)"}g \
	| sed s}QDATDIR}"$(DATDIR)"}g \
	| sed s}QRUNDIR}"$(RUNDIR)"}g \
	| sed s}QUSR}"$(USER)"}g \
	| sed s}QGRP}"$(GROUP)"}g \
	| sed s}QHOMEDIR}"$(HOME)"}g \
	| sed s}QSUDOINCDIR}"`head -1 ac-sudodir`"}g \
	> install
	@chmod 744 install

install: build-stat
	@./install

bindist:
	$(eval export QUBE_BUILD=bindist)
	@$(MAKE) build-stat
	@DESTDIR=$(DESTDIR) $(SHELL) build.in/mk-bindist

libs:
	@cd qlibs ; $(MAKE)

man-pages:
	@echo Creating man pages ...
	@cd man ; $(MAKE)

qemu-adm:
	cat ac-warn.sh qemu-adm.sh \
	| sed s}QETCDIR}"$(ETCDIR)"}g \
	| sed s}QLIBDIR}"$(LIBDIR)"}g \
	| sed s}QUSR}"$(USER)"}g > $@
	chmod 754 $@

qemud-init:
	cat ac-warn.sh $@.sh \
	| sed s}QBINDIR}"$(BINDIR)"}g \
	| sed s}QLIBDIR}"$(LIBDIR)"}g \
	| sed s}QVARDIR}"$(VARDIR)"}g \
	| sed s}DEVNULL}`head -1 ac-devnull`}g > $@
	@chmod 754 $@

qemud-tap:
	cat ac-warn.sh qemud-tap.sh \
	| sed s}DEVNULL}`head -1 ac-devnull`}g > $@
	chmod 750 $@
	ln -sf $@ qemud-`head -1 ac-bridgedev`

qemud-acl: objs qemud-ast
	$(COMPILE) $@.c
	$(LOADBIN) $@ ac-autostart.o ac-libdir.o ac-rundir.o ac-vardir.o \
	ac-hashast.o aclauto.o aclinit.o djbhash.o getextoutput.o \
	usage.o $(QLIBS)
	@chmod 754 $@

qemud-ast: objs
	$(COMPILE) $@.c
	@# we need the hash of qemu-ast.o to be linked with qemud-acl and qemud-ast
	echo "char hashast[] = \"`\`head ac-b2sum\` $@.o | awk '{print $$1;}'`\";" > ac-hashast.c
	$(COMPILE) ac-hashast.c
	$(LOADBIN) $@ ac-autostart.o ac-libdir.o ac-regfile.o ac-hashast.o checkast.o \
	convertname.o djbhash.o isregistered.o usage.o $(QLIBS)
	@chmod 754 $@

qemud-cfg: objs
	$(COMPILE) $@.c monredir.c peripherals.c printconfig.c qemudctrls.c \
	qemuopts.c readconf.c
	$(LOADBIN) $@ ac-regfile.o convbool.o convertname.o getextoutput.o \
	isregistered.o isrunning.o ac-etcdir.o ac-rundir.o monredir.o \
	peripherals.o printconfig.o qemudctrls.o qemuopts.o readconf.o usage.o \
	vncports.o $(QLIBS)
	@chmod 754 $@

qemud-reg: objs
	$(COMPILE) $@.c liststatus.c makepath.c register.c unregister.c
	$(LOADBIN) $@ ac-autostart.o ac-regfile.o ac-usrgrp.o changeowner.o \
	checkast.o convertname.o getextoutput.o isregistered.o isrunning.o \
	ac-libdir.o ac-rundir.o liststatus.o makepath.o register.o \
	unregister.o $(QLIBS)
	@chmod 754 $@

qemud-sio:
	cat ac-warn.sh $@.sh \
	| sed s}QRUNDIR}"$(RUNDIR)"}g \
	| sed s}QLIBDIR}"$(LIBDIR)"}g > $@
	chmod 754 $@

qemud-vnc: objs
	$(COMPILE) $@.c
	$(LOADBIN) $@ ac-regfile.o ac-usrgrp.o ac-etcdir.o ac-libdir.o \
	ac-rundir.o ac-vardir.o convertname.o getextoutput.o isregistered.o \
	isrunning.o usage.o vncports.o $(QLIBS)
	@chmod 754 $@

qemud:
	cat ac-warn.sh qemud.sh \
	| sed s}QETCDIR}"$(ETCDIR)"}g \
	| sed s}QLIBDIR}"$(LIBDIR)"}g > $@
	chmod 754 $@

vncconnect:
	cat ac-warn.sh $@.sh \
	| sed s}VNCFILE}"$(VARDIR)/vncstatus"}g \
	| sed s}DEVNULL}`head -1 ac-devnull`}g > $@
	chmod 754 $@

dot-ssh-qemud:
	cat $@.tpl | sed s}QVARDIR}"$(VARDIR)"}g > $@

sudo-qemud:
	cat $@.tpl \
	| sed s}QLIBDIR}"$(LIBDIR)"}g \
	| sed s}QEMUPATH}`head -1 ac-qemupath`}g \
	| sed s}QGROUP}"$(GROUP)"}g > $@

objs: ac-files
	$(COMPILE) ac-*.c acl*.c
	$(COMPILE) changeowner.c
	$(COMPILE) checkast.c
	$(COMPILE) convbool.c
	$(COMPILE) convertname.c
	$(COMPILE) djbhash.c
	$(COMPILE) getextoutput.c
	$(COMPILE) isregistered.c
	$(COMPILE) isrunning.c
	$(COMPILE) usage.c
	$(COMPILE) vncports.c

ac-files:
	@echo "char home[] = \"$(HOME)\";" > ac-homedir.c
	@echo "char bindir[] = \"$(BINDIR)\";" > ac-bindir.c
	@echo "char etcdir[] = \"$(ETCDIR)\";" > ac-etcdir.c
	@echo "char libdir[] = \"$(LIBDIR)\";" > ac-libdir.c
	@echo "char vardir[] = \"$(VARDIR)\";" > ac-vardir.c
	@echo "char rundir[] = \"$(RUNDIR)\";" > ac-rundir.c
	@echo "char regfile[] = \"$(ETCDIR)/registered\";" > ac-regfile.c
	@echo "char astfile[] = \"$(ETCDIR)/autostart\";" > ac-autostart.c
	@echo "char netscript[] = \"`head -1 ac-netscript`\";" > ac-netscript.c
	@echo "char sysusr[] = \"$(USER)\";" >  ac-usrgrp.c
	@echo "char sysgrp[] = \"$(GROUP)\";" >> ac-usrgrp.c
	@echo "char sumprg[] = \"`head -1 ac-b2sum`\";" > ac-sumprg.c
	@echo "char psprg[] = \"`head -1 ac-ps`\";" > ac-psprg.c

upd-config:
	cat ac-warn.sh $@.sh \
	| sed s}QETCDIR}"$(ETCDIR)"}g > $@
	chmod 754 $@
