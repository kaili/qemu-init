/********************************************************************************
 * isregistered (linkable object file)                                          *
 *                                                                              *
 *  Author: Kai Peter, ©2016-2020                                               *
 * License: ISC                                                                 *
 * Version: 0.3                                                                 *
 *                                                                              *
 * Description: Check if a virtual machine is registered. The Comparison is in  *
 *              lower case always. Return values are 1 (true) or 0 (false).     *
 *******************************************************************************/
#include <unistd.h>
#include <sys/stat.h>
#include "buffer.h"
#include "case.h"
#include "errmsg.h"
#include "getln.h"
#include "open.h"
#include "qstrings.h"

#include "convertname.h"

int IsRegistered(stralloc *sa) {
  struct stat st;
  int fd;
  char buf[128];
  buffer b;
  stralloc line = {0};
  int match;
  int pos;
  stralloc n = {0};
  stralloc p = {0};

  if (stat(regfile,&st) != 0) return(0);      /* first time (new) installation */

  fd = open_read(regfile);
  if (fd == -1) errint(errno,B("Unable to read ",regfile));
  buffer_init(&b,read,fd,buf,sizeof(buf));
  while (1) {
    getln(&b,&line,&match,'\n');
    if (!match) break; /* EOF */
    pos = byte_rchr(line.s,line.len,'\n');               /* cut '\n' if exists */
    if (!stralloc_copys(&line,strsplit(line.s,pos,QLEFT))) errmem;
    if (!stralloc_0(&line)) errmem;

    pos = byte_rchr(line.s,line.len,'/');          /* cut the path (if exists) */
    /* save the real name and the absolute path temporarily */
    if (!stralloc_copys(&n,strsplit(line.s,pos,QRIGHT))) errmem;
    if (!stralloc_0(&n)) errmem;
    if (!stralloc_copys(&p,strsplit(line.s,pos,QLEFT))) errmem;
    if (!stralloc_0(&p)) errmem;

    case_lowers(line.s);
    if (!stralloc_copys(&line,strsplit(line.s,pos,QRIGHT))) errmem;
    if (!stralloc_0(&line)) errmem;
    if (str_equal(line.s,sa->s))
      if (sa->len == line.len) {
        /* if name is registered, save the real name and location from regfile */
        if (!stralloc_copy(&name,&n)) errmem;
        if (!stralloc_copy(&path,&p)) errmem;
        close(fd); return(1);
      }
  }
  close(fd);
  return(0);
}
