/********************************************************************************
 * convbool (linkable object file)                                              *
 *                                                                              *
 *  Author: Kai Peter, ©2020-                                                   *
 * License: ISC                                                                 *
 * Version: 0.2                                                                 *
 *                                                                              *
 * Description: Convert different values to 'true' or return 'false'.           *
 *******************************************************************************/
#define ME ""

#include <unistd.h>
#include "case.h"
#include "qstrings.h"

#include "convbool.h"

unsigned int convBool(char *s) {
  case_lowers(s);
  if (strequal(s,"on"))      return 1;
  if (strequal(s,"y"))       return 1;
  if (strequal(s,"yes"))     return 1;
  if (strequal(s,"1"))       return 1;
  if (strequal(s,"default")) return 1;
  return 0;
}
