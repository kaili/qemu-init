/********************************************************************************
 * isrunning (linkable object file)                                             *
 *                                                                              *
 *  Author: Kai Peter, ©2020-present                                            *
 * License: ISC                                                                 *
 * Version: 0.6.1                                                               *
 *                                                                              *
 * Description: Check if a registered virtual machine is up and running. This   *
 *              could/will be called multiple times by convention,  but it is   *
 *              cheaper that the machine does the work and the code is clean.   *
 *******************************************************************************/
#include <unistd.h>
#include <sys/stat.h>
#include "buffer.h"
#include "errmsg.h"
#include "fd.h"
#include "readclose.h"
#include "qstrings.h"
#include "scan.h"
#include "wait.h"

#include "convertname.h"
#include "getextoutput.h"
#include "isrunning.h"
#include "layout.h"

#include "ac-psprg.c"

stralloc proccmd = {0};     /* output of 'ps' command  */
stralloc pidfile = {0};     /* pidfile of 'name'       */

int IsRunning(void) {
  struct stat st;
  unsigned int pid;    /* pid of the VM */
  stralloc sa = {0};
  int i;

  /* */
  if (!stralloc_copys(&pidfile,rundir)) errmem;
  if (!stralloc_cats(&pidfile,"/")) errmem;
  if (!stralloc_cats(&pidfile,lnam.s)) errmem;
  if (!stralloc_cats(&pidfile,"-qemu.pid")) errmem;
  if (!stralloc_0(&pidfile)) errmem;

  if (stat(pidfile.s,&st) != 0) return(0);    /* no pidfile */
  if (openreadclose(pidfile.s,&sa,64) == -1)
    errint(EPERM,B("Unable to read pidfile:",pidfile.s,"!"));
  if (!stralloc_0(&sa)) errmem;
  scan_ulong(sa.s,(unsigned long *)&pid);

  /* **************************************************************************
  * using the external "ps" command works on most platforms (Linux/(Free)BSD) *
  ************************************************************************** */
  /* first check that a process with the pid from pidfile exists ...
     (thus we prevent invalid output on failure)                             */
  stralloc_init(&proccmd);
  char *c[] = { psprg, "-p", fmtnum(pid), NULL, NULL, NULL };
  getExtOutput(c);       /* returns '0' always or exits itself with an error */
  if(!stralloc_copy(&proccmd,&result)) errmem;
  if(instr(proccmd.s,fmtnum(pid)) == 0) {                 /* no such process */
    unlink(pidfile.s);         /* if it fails - bummer! it is invalid anyway */
    return(0);
  }
  /* ... and get an output from existing - but maybe invalid - process ...   */
  stralloc_init(&proccmd);
  c[3] = "-o"; c[4] = "command";            /* change the "ps" child command */
  getExtOutput(c);       /* returns '0' always or exits itself with an error */
  /* ... but maybe it is another process  (pid from pidfile is still invalid */
  if(!stralloc_copy(&proccmd,&result)) errmem;
  if(instr(proccmd.s,lnam.s) == 0) {   /* pid corresponds to another process */
    unlink(pidfile.s);         /* if it fails - bummer! it is invalid anyway */
    return(0);
  }
  /* now cleanup the valid process command line using temp. stralloc sa */
  stralloc_init(&sa);    /* XXX: important */
  if(!stralloc_copy(&sa,&proccmd)) errmem;         /* make a copy of proccmd */
  while (sa.s[sa.len - 1] == '\n') { sa.len-- ; }   /* remove trailing '\n's */
  if(!stralloc_0(&sa)) errmem;
  /* remove header line from 'ps': BSD doesn't have a '--no-header' option */
  i = instr(sa.s,"\n");
  if(i != sa.len) { stralloc_copys(&sa,strsplit(sa.s,i-1,'R')); }
  if(!stralloc_0(&sa)) errmem;
  if(!stralloc_copy(&proccmd,&sa)) errmem;  /* put the cleanup'ed copy back */

  return((unsigned int)pid);
}
