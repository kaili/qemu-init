/********************************************************************************
 * monredir (linkable object file)                                              *
 *                                                                              *
 *  Author: Kai Peter, ©2020-                                                   *
 * License: ISC                                                                 *
 * Version: 0.8.1                                                               *
 *                                                                              *
 * Description: Configure monitor redirction.                                   *
 *******************************************************************************/
#define ME ""

#include "qstrings.h"

#include "convertname.h"
#include "layout.h"

extern stralloc monitor;

void setMonitor(void) {
  stralloc_copys(&monitor,"");
  /* redirect qemu monitor to unix-sockets: */
  if (!stralloc_cats(&monitor," -monitor unix:")) errmem; /* monsocket */
  if (!stralloc_cats(&monitor,rundir)) errmem;
  if (!stralloc_cats(&monitor,"/")) errmem;
  if (!stralloc_cats(&monitor,lnam.s)) errmem;
  if (!stralloc_cats(&monitor,"-sock.mon,server,nowait,nodelay")) errmem;
  if (!stralloc_cats(&monitor," -monitor unix:")) errmem; /* ctlsocket */
  if (!stralloc_cats(&monitor,rundir)) errmem;
  if (!stralloc_cats(&monitor,"/")) errmem;
  if (!stralloc_cats(&monitor,lnam.s)) errmem;
  if (!stralloc_cats(&monitor,"-sock.ctl,server,nowait,nodelay")) errmem;

  if (!stralloc_0(&monitor)) errmem;
}
