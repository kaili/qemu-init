50000

This is the offset where vnc ports of qemud starts (default: 50000). Beware
of qemu's convention to define vnc ports (see qemu documentation). The off-
set will be added to qemu's internal value (5900).  Thus the real port will
start at <offset> + 5900.
