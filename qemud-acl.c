/********************************************************************************
 * qemud-acl (executable library file)                                          *
 *                                                                              *
 *  Author: Kai Peter, ©2021-present                                            *
 * License: ISC                                                                 *
 *                                                                              *
 * Description: Set ACL's for different files.                                  *
 *******************************************************************************/
#define ME "qemud-acl"
#define VERSION "0.4.1"

#include <unistd.h>
#include <sys/stat.h>
#include "case.h"
#include "errmsg.h"
#include "layout.h"
#include "getoptb.h"
#include "qstrings.h"
#include "usage.h"

extern int autoacls();
extern int initacls();

int main(int argc, char **argv) {
  int opt;
  stralloc parg = {0};      /* positional argument   */
  stralloc popt = {0};      /* argument of option -p */
  char *action = NULL;
  unsigned int count = 0;

  /* Options:
     -s: set acl's for status files (used by 'qemud-init')
     -p: an additional parameter (e.g. pidfile or hash or ...)
     -c: clean: remove files after the guest crashed
     -a: allow disabling of autotstart for members of sysgrp
   */
  while ((opt = getopt(argc,argv,"ap:sUV")) != opteof)
  switch(opt) {
    case 'a': action = "a"; count++; break;
    case 'p': if(!stralloc_copys(&popt,optarg)) errmem; break;
//    case 'r': action = "r"; count++; break;
    case 's': action = "s"; count++; break;
    case 'V': out(B(ME,"-",VERSION)); _exit(0); break;
    default: break;
  }
  argc -= optind;
  argv += optind;

  if(optproblem) _exit(optproblem);
  if(count != 1) errint(ESOFT,"Mutually exclusive arguments!");
  if (!*argv) usage(ME);

  if (!stralloc_copys(&parg,*argv)) errmem;
  if (!stralloc_0(&parg)) errmem;
  case_lowers(parg.s);         /* interactive usage: <name> must be lower case */

  if (!stralloc_0(&popt)) errmem;

  if(*action == 'a') _exit(autoacls(&parg,&popt));
  if(*action == 's') initacls(&parg,&popt);

  _exit(0);
}
