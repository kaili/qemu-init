#********************************************************************************
# qemud - simple wrapper script to start multiple qemu virtual machines         *
#                                                                               *
#  Author: Kai Peter, ©2016-present                                             *
# Version: 0.41                                                                 *
# License: ISC                                                                  *
#                                                                               *
# Description: qemud is a wrapper around qemu-init to start/stop multiple VM's. *
#              Commonly qemud will be called by the init system.                *
#********************************************************************************
VERSION=`cat $0 | grep -m 1 'Version' | cut -d\   -f3`       # get version number
ME=`basename $0`                                             # set program name

usage() { echo "Usage: "`basename $0`" start|stop|status" ; exit 1 ; }

[ $1 ] || usage
[ $2 ] && usage
( [ $1 = "-V" ] || [ $1 = "--version" ] ) && echo "$ME-$VERSION" && exit 0
[ $1 = "start" ] || [ $1 = "stop" ] || [ $1 = "status" ] || usage
if [ $1 = "status" ] ; then QLIBDIR/qemud-reg -s ; exit $? ; fi

if [ `id -u` != 0 ] ; then
   printf "\033[91merror:\033[0m\033[1m only `id -nu 0` can $1 _all_"
   printf " registered virtual machines!\n\033[0m"
   exit 1
fi

autostart() {
  [ "$1" = "start" ] || return
  # exit silently if file 'autostart' doesn't exists
  [ ! -f "QETCDIR/autostart" ] && exit 0
  [ -n "$VM" ] && VM=`grep "$VM" QETCDIR/autostart` || VM=""
}

CFF="QETCDIR/registered"
[ ! -f "$CFF" ] && echo "Config file '$CFF' not found! Aborting!" && exit 4

# we want to shutdown registered VM's in reverse order
[ $1 = "stop" ] && tac "$CFF" > /tmp/qemud-stop && CFF="/tmp/qemud-stop"

export QEMUINIT="$ME"               # required to control behavior of 'qemu-init'
while IFS= read L ; do
  VM=$(echo "$L" | grep -v '^#' | tr '[A-Z]' '[a-z]') ; VM=`basename "$VM"`
  autostart "$1"
  # Never call 'qemu-init' as a background job here! 'qemu-init' puts itself into
  # background if required (the $BOOTDELAY option is set).
  if [ -n "$VM" ] ; then QLIBDIR/qemud-init "$VM" "$1" ; fi
done < "$CFF"
