/********************************************************************************
 * qemud-vnc (executable library)                                               *
 *                                                                              *
 *  Author: Kai Peter, ©2020-                                                   *
 * License: ISC                                                                 *
 * Version: 0.3                                                                 *
 *                                                                              *
 * Description: Handle vnc connections.                                         *
 *******************************************************************************/
#define ME "qemud-vnc"
#define VERSION "0.3"

#include <unistd.h>
#include <netdb.h>
#include <sys/stat.h>
#include "buffer.h"
#include "case.h"
#include "env.h"
#include "getln.h"
#include "getoptb.h"
#include "open.h"
#include "readclose.h"
#include "qstrings.h"

#include "convertname.h"
#include "getextoutput.h"
#include "isrunning.h"
#include "usage.h"
#include "vncports.h"

#include "layout.h"

stralloc vncfile = {0};
stralloc vnchost = {0};

unsigned int vncport = 0;

stralloc *vncHost() {
  char host[256];
  struct hostent* h;

  host[0] = 0; /* sigh */
  gethostname(host,sizeof(host));
  host[sizeof(host) - 1] = 0;
  h = gethostbyname(host);
  if (!stralloc_copys(&vnchost,h->h_name)) errmem;
  if (!stralloc_0(&vnchost)) errmem;
  case_lowers(vnchost.s);
  return(&vnchost);  /* prevent senseless compiler warning (gcc) */
}

void listVNC() {
  struct stat st;
  stralloc sa = {0};
  int pos;

  /* simply stop with an error for 'list' here - don't try to link */
  if (stat(vncfile.s,&st) != 0) errint(errno,vncfile.s);

  if (!openreadclose(vncfile.s,&sa,64)) _exit(0);
  /* remove header and the empty line (first two lines) ... */
  stralloc_copys(&sa,strsplit(sa.s,instr(sa.s,"\n"),'R'));
  stralloc_0(&sa);
  /* ... as well the last '\n' */
  pos = byte_rchr(sa.s,sa.len,'\n');
  sa.s[pos] = '\0';
  out(sa.s);
  _exit(0);
}

void writeVNC() {
  struct stat st;
  int fd;
  stralloc line = {0};
  int match;
  buffer b;
  char buf[128];
  int pos;
  stralloc sa = {0};

  vncHost();

  if (chdir(vardir) == -1) errint(errno,B("Unable to change into ",vardir,"!"));
  if (stat(vncfile.s,&st) != 0) {
    /* The 'vncfile' _MUST_ exist! The _real_ 'vncfile' is "delete protected" in
       the 'etcdir' (sticky bit is set on 'etcdir'), but we work on a (hard)link
       in 'vardir'. Thus we create the link if not exists (at write only).    */
    if(!stralloc_copys(&sa,etcdir)) errmem;
    if(!stralloc_cats(&sa,"/vncstatus")) errmem;
    if(!stralloc_0(&sa)) errmem;
    if(link(sa.s,"vncstatus") == -1)
      errint(errno,B("Couldn't create ",vncfile.s,"!"));
    stralloc_init(&sa);
  }
  /* get all registered machines from regfile ... */
  fd = open_read(regfile);
  buffer_init(&b,read,fd,buf,sizeof(buf));
  /* add a header to file 'vncstatus' */
  char t[] = "# This file was created by 'install' and MUST EXIST at all!\n\n";
  if (!stralloc_copys(&sa,t)) errmem;
  while(1) {
    getln(&b,&line,&match,'\n');
    if (!match) break; /* EOF */
    line.s[byte_chr(line.s,line.len,'\n')] = '\0';
    pos = byte_rchr(line.s,line.len,'/');
    stralloc_copys(&name,strsplit(line.s,pos,'R'));
    stralloc_0(&name);
    stralloc_copys(&path,strsplit(line.s,pos,'L'));
    stralloc_0(&path);
    if (name.len > 1) {           /* exclude possible invalid lines in regfile */
      convertName(&name);
      if (IsRunning(&name) != 0)
        if (instr(proccmd.s,"-display vnc=:")) {
          vncport = initvnc(lnam);
          vncport = vncport + 5900; /* here we have to write the real vnc port */
          if (!stralloc_cats(&sa,lnam.s)) errmem;
          if (!stralloc_cats(&sa,":")) errmem;
          if (!stralloc_cats(&sa,vnchost.s)) errmem;
          if (!stralloc_cats(&sa,":")) errmem;
          if (!stralloc_cats(&sa,fmtnum(vncport))) errmem;
          if (!stralloc_cats(&sa,"\n")) errmem;
        }
    }
  }
  close(fd);

  if (!stralloc_0(&sa)) errmem;

  fd = open_trunc(vncfile.s);
  if (fd == -1) errint(errno,B("Couldn't open ",vncfile.s,"!"));
  buffer_init(&b,write,fd,buf,sizeof(buf));
  buffer_putsflush(&b,sa.s);
  close(fd);

  _exit(0);
}

int main(int argc, char **argv) {
  int opt;
  int action = 0;
  stralloc parg = {0};    /* positional argument (required, except for "list") */
  int pid;

  while ((opt = getopt(argc,argv,"hVw")) != opteof)
  switch(opt) {
    case 'h': usage(B(ME," [ -w ] |")); break;
    case 'w': action = 'w'; break;
    case 'V': version(ME,VERSION); break;
    default: action = 0; break;
  }
  argc -= optind;
  argv += optind;

  if (!stralloc_copys(&vncfile,vardir)) errmem;
  if (!stralloc_cats(&vncfile,"/vncstatus")) errmem;
  if (!stralloc_0(&vncfile)) errmem;

  if ((!*argv) && (!action)) listVNC(); /* list and exit */
  if (action == 'w') writeVNC();        /* write to 'vncstatus' and exit */

  if(!stralloc_copys(&parg,*argv)) errmem;
  if(!stralloc_0(&parg)) errmem;

  convertName(&parg);
  stralloc_free(&parg);

  if (!IsRegistered(&lnam))
    errint(EINVAL,B("Not a registered virtual machine: ",name.s));

  if (!IsRunning(&lnam)) {
    log(B("\033[1m",name.s,"\033[0m is not running!"));
    _exit(ESOFT);
  }

  if (!instr(proccmd.s,"-display vnc=:"))
    errsoft("no vnc enabled");

  if (chdir(libdir) == -1)
    errint(errno,B("Unable to change into ",libdir,"!"));
  char *(c[3]);
  c[0] = "./vncconnect";
  c[1] = lnam.s;
  c[2] = 0;
  pid = fork();
  if (!pid) { execvp(*c,c); if (errno) errint(errno,ME); }
  _exit(0);
}
