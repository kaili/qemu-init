/********************************************************************************
 * usage (linkable opbject file)                                                *
 *                                                                              *
 *  Author: Kai Peter, ©2020-                                                   *
 * License: ISC                                                                 *
 * Version: 0.1                                                                 *
 *                                                                              *
 * Description: Standard usage message.                                         *
 *******************************************************************************/
#define ME ""

#include <unistd.h>
#include "errmsg.h"
#include "usage.h"

void usage(char *me) {
  errint(EINVAL,B("usage: ",me," <name>"));
}

void version(char *me,char *version) {
  out(B(me,"-",version));
  _exit(0);
}
