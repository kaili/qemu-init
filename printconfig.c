/********************************************************************************
 * printconfig (linkable object file)                                           *
 *                                                                              *
 *  Author: Kai Peter, ©2020-                                                   *
 * License: ISC                                                                 *
 * Version: 0.2                                                                 *
 *                                                                              *
 * Description: Print the commandline for 'name' well-arranged.                 *
 *******************************************************************************/
#define ME "printconfig"

#include <unistd.h>
#include "buffer.h"
#include "qstrings.h"

#include "convertname.h"
#include "isrunning.h"

void printConfig(stralloc cmdln, int type) {
  stralloc sa = {0};
  int pos = 0;
  int pid;
  int i = 0;

  if (!stralloc_copys(&sa,"")) errmem;

  pos = instr(cmdln.s,"-usb -device usb");
  for (;;) {
    if (pos == 0) break;
    cmdln.s[pos+3] = 'Z';
    pos = instr(cmdln.s,"-usb -device usb");
  }

  pos = instr(cmdln.s,"-usb -usbdevice ");  /* deprecated qemu notation */
  for (;;) {
    if (pos == 0) break;
    cmdln.s[pos+3] = 'Z';
    pos = instr(cmdln.s,"-usb -usbdevice ");
  }

  for (pos=0;pos<cmdln.len;pos++) {
    if (cmdln.s[pos] == ' ')
      if (cmdln.s[pos+1] == '-') {
        cmdln.s[pos] = '\n';
    }
  }
  if (!stralloc_copys(&cmdln,chrpls(cmdln.s,'Z',' '))) errmem;
  if (!stralloc_0(&cmdln)) errmem;

  while(1) {              /* a little bit output formatting ... */
    pos = byte_chr(cmdln.s,cmdln.len,'\n');
    if (pos <= 1) break;
    if (!stralloc_cats(&sa,strsplit(cmdln.s,pos,QLEFT))) errmem;
    if (!stralloc_cats(&sa,"\n")) errmem;
    if (!stralloc_cats(&sa,"\t")) errmem;
    if (!stralloc_copys(&cmdln,strsplit(cmdln.s,pos,QRIGHT))) errmem;
    if (!stralloc_0(&cmdln)) errmem;
    i++;
  }
  pos = byte_rchr(sa.s,sa.len,'\t');   /* remove trailing '\t' */
  if (!stralloc_copys(&sa,strsplit(sa.s,pos,QLEFT))) errmem;
  if (!stralloc_0(&sa)) errmem;

  buffer_puts(buffer_1,"\033[1m");
  if (!type) {  /* XXX: type: 0 = config, 1 = runtime */
    buffer_puts(buffer_1,"Configuration as it would be right now:\033[0m\n");
  } else {
    buffer_puts(buffer_1,"Actual runtime configuration:\033[0m\n");
  }
  buffer_puts(buffer_1,sa.s);
  buffer_puts(buffer_1,"\n");

  pid = IsRunning(name);
  if (!pid) {
    buffer_puts(buffer_1,name.s);
    buffer_puts(buffer_1," is \033[93mnot running\033[0m.\n");
    buffer_puts(buffer_1,"(TCP ports are calculated as it would be right now.)");
  } else {
    buffer_puts(buffer_1,name.s);
    buffer_puts(buffer_1," \033[32mis running\033[0m with pid ");
    buffer_puts(buffer_1,fmtnum(pid));
    if (!type)
      buffer_puts(buffer_1,".\n(This could be different from the actual runtime configuration.)");
  }
  buffer_puts(buffer_1,"\n");
  buffer_flush(buffer_1);
}
