/********************************************************************************
 * peripherals (linkable object file)                                           *
 *                                                                              *
 *  Author: Kai Peter, ©2020-                                                   *
 * License: ISC                                                                 *
 * Version: 0.3                                                                 *
 *                                                                              *
 * Description: Build peripheral hardware: harddisks, optical drives (cdrom's)  *
 *              and network adapters.                                           *
 *******************************************************************************/
#define ME "periphals"

#include <unistd.h>
#include <sys/stat.h>
#include "errmsg.h"
#include "qstrings.h"

#include "convertname.h"
#include "readconf.h"

#define TO_HEX(h) (h <= 9 ? '0' + h : 'A' - 10 + h)  /* MAC address conversion */
#include "ac-netscript.c"      /* start script(s) for tap devices (qemu-netif) */

unsigned int n = 0;    /* counter for NIC's */

unsigned short int createMAC(const char nicids[]) {
  char ch;
  int i;
  unsigned int s;            /* to store (signed) ch as __unsigned__ int */
  const unsigned int w = 62; /* weight: all upper and lower case letters + 0 to 9 */
  unsigned short x = 0;      /* calc: 'char ch as int' * 'no* of alpha' * 'length of name' */

  for (i=0;i<str_len(nicids);i++) {
    ch = nicids[i];
    s = ch;   /* store ch as unsigned int */
    if (ch >= '0') s = (ch - '0');
    if (ch >= 'A') s = (ch - 'A' + 10);      /* + numbers */
    if (ch >= 'a') s = (ch - 'a' + 10 + 26); /* + numbers + upper case letters */
    x += s + (i * w);
  }
  return(x);
}

/* Get (multiple) standard disks, optical drives and network cards */
void getPeripherals(char *key, char *value) {
  char mac[4];
  stralloc sa = {0};
  struct stat st;

  if (str_len(value) == 0) return;          /* don't add an "empty" peripheral */

  if (str_pcmp(key,"DSK")) {
    if (!stralloc_cats(&stdopts," -drive file=")) errmem;
    if (!instr(value,"/")) {
      if (!stralloc_cats(&stdopts,path.s)) errmem;
      if (!stralloc_cats(&stdopts,"/")) errmem;
    }
    if (!stralloc_cats(&stdopts,value)) errmem;
    if (!stralloc_cats(&stdopts,",snapshot=off")) errmem;
  }
  if (str_pcmp(key,"CDR")) {
    if (!stralloc_cats(&stdopts," -drive media=cdrom")) errmem;
    if (!instr(value,"/")) {
      if (!stralloc_cats(&sa,path.s)) errmem;
      if (!stralloc_cats(&sa,"/")) errmem;
      if (!stralloc_cats(&sa,value)) errmem;
      if (!stralloc_0(&sa)) errmem;
    }

    /* if the cd image could not be stat'ed add an empty drive */
    if (stat(sa.s,&st) != 0) return;

    if (!stralloc_cats(&stdopts,",file=")) errmem;
    if (!stralloc_cats(&stdopts,sa.s)) errmem;
  }
  if (str_pcmp(key,"NIC")) /* network adapters */ {
    if (n > 99) {  /* by convention: lenght of name + number of NIC's */
      errint(WARN,"The maximum number (100) of network adapters was reached!");
      return;
    }
    if (!stralloc_cats(&stdopts," -device ")) errmem;
    if (!stralloc_cats(&stdopts,value)) errmem;
    if (!stralloc_cats(&stdopts,",netdev=net")) errmem;
    if (!stralloc_cats(&stdopts,fmtnum(n))) errmem;
    if (!stralloc_cats(&stdopts,",mac=DE:AD:BE:EF:")) errmem;
    /* create the last 2 bytes of the MAC address from 'name + n' */
    if (!stralloc_copys(&sa,name.s)) errmem;
    if (!stralloc_cats(&sa,fmtnum(n))) errmem;
    if (!stralloc_0(&sa)) errmem;

    unsigned short int x = createMAC(sa.s);
    mac[0] = TO_HEX(((x >> 12) & 0x0F));
    if (!stralloc_catb(&stdopts,&mac[0],1)) errmem;
    mac[1] = TO_HEX(((x >> 8) & 0x0F));
    if (!stralloc_catb(&stdopts,&mac[1],1)) errmem;
    if (!stralloc_cats(&stdopts,":")) errmem;
    mac[2] = TO_HEX(((x >> 4) & 0x0F));
    if (!stralloc_catb(&stdopts,&mac[2],1)) errmem;
    mac[3] = TO_HEX((x & 0x0F));
    if (!stralloc_catb(&stdopts,&mac[3],1)) errmem;

    if (!stralloc_cats(&stdopts," -netdev tap,id=net")) errmem;
    if (!stralloc_cats(&stdopts,fmtnum(n))) errmem;
    if (!stralloc_cats(&stdopts,",script=")) errmem;
    if (!stralloc_cats(&stdopts,netscript)) errmem;
    n++;
  }
}
