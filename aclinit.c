/********************************************************************************
 * aclinit (linkable object file)                                               *
 *                                                                              *
 *  Author: Kai Peter, ©2021-present                                            *
 * Version: 0.2.1                                                               *
 * License: ISC                                                                 *
 *                                                                              *
 * Description: Set ACL's for different status files of a virtual machine.      *
 *******************************************************************************/
#include <unistd.h>
#include <sys/stat.h>
#include "errmsg.h"
#include "layout.h"
#include "qstrings.h"
#include "stralloc.h"
#include "wait.h"

int initacls(stralloc *name,stralloc *pidfile) {
  stralloc fn = {0};
  stralloc sa = {0};
  struct stat st;
  int pid = 0;
  int wstat;
  char *c = "setfacl -m g::rw- ";        /* default command used by qemud-init */

  if(chdir(rundir) == -1) errint(errno,rundir);
  /* set acl's of file <name>-sock.ctl */
  if(!stralloc_copys(&fn,name->s)) errmem;
  if(!stralloc_cats(&fn,"-sock.ctl")) errmem;
  if(!stralloc_0(&fn)) errmem
  if(stat(fn.s,&st) == 0) {
    if(!stralloc_copys(&sa,c)) errmem;
    if(!stralloc_cats(&sa,fn.s)) errmem;
    if(!stralloc_0(&sa)) errmem;
    pid = fork();
    if (!pid) execlp("bash","bash","-c",sa.s, (char *)NULL);
    wait_pid(&wstat,pid);
  }
  /* set acl's of file <name>-sock.mon */
  if(!stralloc_copys(&fn,name->s)) errmem;
  if(!stralloc_cats(&fn,"-sock.mon")) errmem;
  if(!stralloc_0(&fn)) errmem;
  if(stat(fn.s,&st) == 0) {
    if(!stralloc_copys(&sa,c)) errmem;
    if(!stralloc_cats(&sa,fn.s)) errmem;
    if(!stralloc_0(&sa)) errmem;
    pid = fork();
    if (!pid) execlp("bash","bash","-c",sa.s, (char *)NULL);
    wait_pid(&wstat,pid);
  }
  /* set acl of pidfile (read by group) */
  if(stat(pidfile->s,&st) == 0) {
    if(!stralloc_copys(&sa,"setfacl -m g::r-- ")) errmem
    if(!stralloc_cats(&sa,pidfile->s)) errmem;
    if(!stralloc_0(&sa)) errmem;
    pid = fork();
    if (!pid) execlp("bash","bash","-c",sa.s, (char *)NULL);
    wait_pid(&wstat,pid);
  }
  /* set acl of startup error file /<name>-error.log) */
  if(chdir(vardir) == -1) errint(errno,vardir);
  if(!stralloc_copys(&fn,name->s)) errmem;
  if(!stralloc_cats(&fn,"-error.log")) errmem;
  if(!stralloc_0(&fn)) errmem
  if(stat(fn.s,&st) == 0) {
    if(!stralloc_copys(&sa,c)) errmem;
    if(!stralloc_cats(&sa,fn.s)) errmem;
    if(!stralloc_0(&sa)) errmem;
    pid = fork();
    if (!pid) execlp("bash","bash","-c",sa.s, (char *)NULL);
    wait_pid(&wstat,pid);
  }
  _exit(0);
}
