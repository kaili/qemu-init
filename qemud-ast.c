/********************************************************************************
 * qemud-ast (executable file)                                                  *
 *                                                                              *
 *  Author: Kai Peter, ©2016-present                                            *
 * License: ISC                                                                 *
 *                                                                              *
 * Description: Read and write the config file 'autostart'.                     *
 *******************************************************************************/
#define ME "qemud-ast"
#define VERSION "0.7.3"

#include <unistd.h>
#include <sys/stat.h>
#include "buffer.h"
#include "case.h"
#include "errmsg.h"
#include "getln.h"
#include "open.h"
#include "qstrings.h"
#include "readclose.h"
#include "wait.h"

#include "autostart.h"
#include "convertname.h"
#include "djbhash.h"
#include "layout.h"
#include "usage.h"

buffer b;
char buf[256];

int setACL(char *a) {
  int wstat;

  unsigned long h = djbhash(fmtnum(getpid()));  /* create a hash from getpid() */

  char *c[] = { "sudo","./qemud-acl","-a","-p",fmtnum(h), "write",(char *)NULL };
  if (*a == 'R') c[5] = "read";    /* restore access rights */
  int pid = fork();
  if (!pid) execvp(*c,c);
  if(wait_pid(&wstat,pid) != 0) return(wstat);
  if(errno) return(errno);
  return(0);
}

int enableAutostart(void) {
  int fd;

  if(getuid() != 0)          /* 'root' privileges required to enable autostart */
    errint(EPERM,"privilege escalation required to enable 'autostart'!");

  fd = open_append(astfile);
  if (fd < 0) errint(errno,B("Couldn't open ",astfile,"!"));
  buffer_init(&b,write,fd,buf,sizeof(buf));
  buffer_puts(&b,lnam.s);  /* XXX: lower case name only */
  buffer_puts(&b,"\n");
  buffer_flush(&b);
  close(fd);
  out(B("Enabled autostart for ",name.s,"."));
  return(0);
}

int disableAutostart(void) {
  stralloc sa = {0};
  stralloc dup = {0};
  int pos;
  int fd;
  char *x;

  /* make config file 'autostart' writable by group  */
  if(getuid() != 0) if(setACL("W") != 0) errint(EPERM,astfile);

  if(!openreadclose(astfile,&sa,64))  /* read the file into temporary stralloc */
    errsoft(B("Unable to read ",astfile," - aborting!"));
  fd = open_trunc(astfile);
  if (fd < 0) errint(errno,B("Couldn't open ",astfile,"!"));
  buffer_init(&b,write,fd,buf,sizeof(buf));
  if (!stralloc_copys(&dup,"")) errmem;

  /* loop through the temp. stralloc */
  for(;;) {
    pos = byte_chr(sa.s,sa.len,'\n');
    x = strsplit(sa.s,pos,'L');
    case_lowers(x);       /* XXX: make sure it is in lower case */
    if (!stralloc_copys(&sa,strsplit(sa.s,pos,'R'))) errmem;
    /*  */
    if (str_diff(x,lnam.s))
      if (!instr(dup.s,x)) {    /* prevent duplicates */
        if (!stralloc_cats(&dup,x)) errmem;
        buffer_puts(&b,x);
        buffer_puts(&b,"\n");
        buffer_flush(&b);
      }

    if (sa.len <= 1) break;
    if (!stralloc_0(&sa)) errmem;
  }
  close(fd);
  stralloc_free(&sa);

  if(getuid() != 0)        /* restore acl: remove group writeable bit */
    if(setACL("R") != 0) errlog(0,WARN,B("Wrong permissions: ",astfile));

  out(B("Disabled autostart for ",name.s,"."));
  return(0);
}

int chgAutostart(void) {
  struct stat st;
  int fd;
  int ret;

  if (stat(astfile,&st) != 0) {         /* create config file if doesn't exist */
    fd = open_append(astfile);
    if (fd < 0) errint(errno,B("Couldn't create ",astfile,"!"));
    buffer_init(&b,write,fd,buf,sizeof(buf));
    if(buffer_puts(&b,"# This file is programmatically written.") != 0) errmem;
    if(buffer_puts(&b," Do not edit!\n") != 0) errmem;
    if(buffer_flush(&b) != 0) errsys(errno);
    close(fd);
    if (chmod(astfile,0640) != 0) errint(errno,astfile);
  }

  ret = hasAutostart(lnam.s);
  switch (ret) {
    case 0: enableAutostart(); break;
    case 1: disableAutostart(); break;
    case 2: log(B("Found ",name.s," but something was wrong. Please check ",astfile,"!")); break;
    case -1: errint(ENOENT,B("Unable to read ",astfile,"!")); break;
  }
  stralloc_free(&as);   /* the stralloc 'as' is invalid after changes */
  return(0);
}

int main(int argc,char **argv) {
  stralloc parg = {0};

  if (!argv[1]) usage(ME);
  if (strequal(argv[1],"-V")) version(ME,VERSION);
  if (argv[2]) usage(ME);
  if (!stralloc_copys(&parg,argv[1])) errmem;
  if (!stralloc_0(&parg)) errmem;
  convertName(&parg);

  if (!IsRegistered(&lnam))
    errint(EINVAL,B("Not a registered virtual machine: ",name.s));

  chgAutostart();
  _exit(0);
}
