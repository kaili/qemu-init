#ifndef QEMUOPTS_H
#define QEMUOPTS_H

extern stralloc optname;
extern stralloc optdesc;

extern char *qemubin;   /* new: tgtarch */

extern stralloc runas;
extern int kvm;
extern int dmn;         /* daemonize */
extern char cpu[];
extern stralloc mem;

extern stralloc display;
extern char vnckeyb[];

#endif
