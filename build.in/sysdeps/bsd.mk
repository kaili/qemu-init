#********************************************************************************
# Define equivalent values for _all_ BSD systems                                *
#********************************************************************************
USERSHELL = `head -1 ac-shell`

# package dependant
BRIDGEDEV = bridge0     # usually not used on (Free)BSD, but by the build process
NETSCRIPT = no          # on (Free)BSD we don't have to use a net script
