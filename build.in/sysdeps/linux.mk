#********************************************************************************
# Set equivalent values for linux systems                                       *
#********************************************************************************
USERSHELL = `head -1 ac-shell`

# package dependant
BRIDGEDEV = br0
NETSCRIPT = `grep ^LIBDIR: ac-layout | cut -d: -f2`/qemud-$(BRIDGEDEV)

PWLOCKUSR = usermod -L
