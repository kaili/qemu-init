#********************************************************************************
# Define special values for OpenBSD systems                                     *
#********************************************************************************

include $(QUBEDIR)/sysdeps/bsd.mk

PWLOCKUSR = usermod -Z
