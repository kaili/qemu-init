#!/config/sh                                   # --> for syntax highlighting only
#********************************************************************************
# build parameters of qemu-init - This file will be used by configure.          *
#                                                                               *
# Version: 20220608                                                             *
#********************************************************************************

#********************************************************************************
# [layout section]
# Define the directory layout and some initial variables (defaults) which will be
# used by the corresponding  'layout-<name>'  file. Each variable defined in this
# section could be overwritten in 'conf-layout' - even separatly or all together.
LAYOUT="upm"

# PKGDIR is synomym with the home dir of the user(s). The value here takes effect
# only, if the user(s) doesn't exist. Otherwise the home dir of the user from the
# 'password' file will be used.
PKGDIR="/home/srvcs/qemud"

#********************************************************************************
# A general prefix to be used as variable in layout files.
PREFIX="/usr/local"
# The variable SUFFIX could/will be appended to files or directory names. Usually
# it will be used in layout files.
SUFFIX="qemud"

#********************************************************************************
# [privileges section]
# The effective user (default: 'qemud') and group (default: 'kvm'). Changing this
# could result in undefined behavior, especially on updates - be careful!
USR="qemud:::$PKGDIR"
GRP=kvm:78

# [compile section]   ***********************************************************
# This will be used to compile '.c' files. The defaults can be overwritten with a
# file 'conf-cc' and extended through a file 'conf-ccopts'.
CC="cc -O2 -Wall"

# This will be used to link '.o' files into an executable.  The defaults could be
# overwritten with a file 'conf-ld' and extended through 'conf-ldopts'.
LD="cc -s"

# Set additional flags for the compiler and linker. The CCOPTS/LDOPTS will be ap-
# pended to CC/LD. Usually these vars will be left empty here.
CCOPTS=
LDOPTS=

# [install section]   ***********************************************************

#********************************************************************************
# [misc section]
# Here the developer(s) can define  additional variables required by the package.
# The corresponding code to evaluate it should go into 'package.inc'.
# (Packagers should use 'conf-*' files to set variables for their distribution.)
SUDOERSDIR="/etc/sudoers.d"
