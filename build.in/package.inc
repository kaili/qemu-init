#!/config/sh                              # just and only for syntax highlighting
#********************************************************************************
# qware build environment - qube extension (part of configure)                  *
#                                                                               *
#  Author: Kai Peter, ©2022-present                                             *
# Version: 0.2 (template version)                                               *
# License: ISC                                                                  *
#                                                                               *
# Description: This is the place for developers to extend configure to do real  *
#              package specific stuff not covered by qube. It will be executed  *
#              as the last step. It have to be named 'package.inc'.             *
#********************************************************************************

# get the path of "qemu-system-*", assuming it's in the same folder like qemu-img
bash -c "echo $(dirname `which qemu-img 2>$NIL` 2>$NIL) > ac-qemupath"
grep -q ^/ ac-qemupath
#[ $? -ne 0 ] && echo -e "\033[1mUnable to find qemu! Aborting!\033[0m" && exit 1
[ $? -ne 0 ] && echo -e "\033[93mWarning: Unable to find qemu!\033[0m"


# determine the sudo includedir:
# we can't get it w/o root privileges from 'sudo -V', thus we try common dirs
log "Checking for sudoers includedir ... "
for D in /etc/sudoers.d \
         /qlnx/etc/sudoers.d \
         /usr/local/etc/sudoers.d
do  [ -d $D ] && log "$D (OK)\n" && break ; done
# maybe it is some where in the conf-* files
[ -d "$SUDODIR" ] && D="$SUDODIR"

if [ ! -d "$D" ] ; then
  D="/etc/sudoers.d"        # using hard fallback
  log "not found!\n"
  printf "\t\033[33m"; log "Warning: Using hard fallback /etc/sudoers.d\033[0m\n"
fi
echo "$D" > ac-sudodir
