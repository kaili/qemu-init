#********************************************************************************
# qemud-tap - bring up a bridged network interface for qemu-init                *
#                                                                               *
#  Author: Kai Peter, ©2016-present                                             *
# Version: 0.43                                                                 *
# License: ISC                                                                  *
#                                                                               *
# Description: qemu-netif will be used by qemu-init to start/stop the network   *
#              interface(s).  It will be called through either qemu-brdown or   *
#              qemu-brup symlinks - never directly.                             *
#              The parameter '$1' is the device name from qemu, e.g. 'tap2'.    *
#********************************************************************************
VERSION=`cat $0 | grep -m 1 'Version' | cut -d\   -f3`       # get version number
ME=`basename $0`                                             # set program name
BRIDGEDEV=`echo $ME | cut -d- -f2`

down() {
  ip link set $1 down
  ip link set dev $1 nomaster
  ip link delete dev $1
  exit $?
}

start() {
  ip tuntap add $1 mode tap user `whoami` 2>DEVNULL
  ip link set $1 up
  sleep 0.5s
  ip link set $1 master "$BRIDGEDEV"
  exit $?
}

[ "$1" = "-V" ] && echo "$ME-VERSION" && exit 0
# 'qemud-tap' shouldn't be called by its own name ...
[ "$ME" = "qemud-tap" ] && echo invalid call && exit 1
# ... but through a symlink
[ "$ME" = "qemud-$BRIDGEDEV" ] && start $1 || exit 1
