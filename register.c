/********************************************************************************
 * register (linkable object file)                                              *
 *                                                                              *
 *  Author: Kai Peter, ©2016-present                                            *
 * License: ISC                                                                 *
 * Version: 0.5.2                                                               *
 *                                                                              *
 * Description: Register a virtual machine for usage with qemu-init.            *
 *******************************************************************************/
#define ME "register"

#include <unistd.h>
#include <sys/stat.h>
#include "buffer.h"
#include "case.h"
#include "env.h"
#include "errmsg.h"
#include "getln.h"
#include "qstrings.h"
#include "open.h"
#include "wait.h"

#include "changeowner.h"
#include "convertname.h"
#include "register.h"
#include "makepath.h"

#include "ac-homedir.c"

/* option flags */
extern int flag_user;

stralloc dst = {0};
char buf[128];
buffer b;

ssize_t mywrite(int fd,char *buf,int len) {
  buffer_put(&b,buf,len);
  return len;
}

int createFile(stralloc *sa) {
  int wstat;

  umask(002);
  int pid = fork();
  if (!pid)
    execlp("bash", "bash" , "-c", sa->s, (char *) NULL);
  wait_pid(&wstat,pid);
  if (errno) errint(errno,B("Couldn't create ",dst.s,"!"));
  if (chmod(dst.s, 0664) !=0) errint(errno,B("Unable to chmod ",dst.s));
  /* change owner with '-U' */
  if (flag_user)
    if (chgOwner(dst.s,"") != 0)
      errint(errno,B("Couldn't create ",dst.s," (ownership)!"));

  return(0);
}

int doRegister(void) {
  int fd;
  char *fn = regfile;
  int pos;
  stralloc sa = {0};    /* temporary variable */
  stralloc src = {0};   /* hold config file template names */
  struct stat st;

  /* if no path is given, use the default location (path.s contains '\0' only */
  if (path.len == 1) {
    if (!stralloc_copys(&path,home)) errmem;
    if (!stralloc_cats(&path,"/dat")) errmem;
    if (!stralloc_0(&path)) errmem;
  }

  /* prepend 'HOMEDIR/dat' to relative path */
  if (path.s[0] != '/') {
    stralloc_init(&sa);
    if (!stralloc_copys(&sa,home)) errmem;
    if (!stralloc_cats(&sa,"/dat/")) errmem;
    if (!stralloc_cat(&sa,&path)) errmem;
    if (!stralloc_0(&sa)) errmem;
    stralloc_init(&path);
    if (!stralloc_copy(&path,&sa)) errmem;
  }

  /* remove trailing slash'es from 'path' (required with option '-p' only) */
  for(;;) {
    if (path.s[str_len(path.s) - 1] != '/') break;
    pos = byte_rchr(path.s,str_len(path.s),'/');
    if (!stralloc_copys(&path,strsplit(path.s,pos,QLEFT))) errmem;
    if (!stralloc_0(&path)) errmem;
  }

  if (str_equal(name.s,path.s)) errint(EINVAL,B("Invalid name: '",name.s,"'!"));
  if (IsRegistered(&lnam) == 1)
    errint(EEXIST,B("A virtual machine called ",name.s," is registered already!"));

  /* handle absolute path outside the homedir */
  if (stat(path.s,&st) != 0) {
    log(B("Creating folder ",path.s));
    if (stat(path.s,&st) != 0) {
    if (mkpath(path.s,0775) != 0) errsys(errno);
    if (chmod(path.s,02775) != 0) errint(errno,path.s);  /* group privileges */
    }
    if (flag_user) {     // ???????????
      if (!stralloc_copys(&sa,sysusr)) errmem;
    } else {
      if (!stralloc_copys(&sa,env_get("USER"))) errmem;
    }
    if(!stralloc_cats(&sa,".")) errmem;
    if(!stralloc_cats(&sa,sysgrp)) errmem;
    if(!stralloc_0(&sa)) errmem;
    if (chgOwner(path.s,sa.s) != 0)
      errint(errno,B("Unable to change ownership of ",path.s,"!"));
  }
  if (chdir(path.s) == -1) errint(errno,B("Unable to switch to ",path.s,"!"));

  /* create '*-qemud.rc' file from '$HOME/etc/template-qemud.rc' */
  if (!stralloc_copys(&dst,lnam.s)) errmem;
  if (!stralloc_cats(&dst,"-qemud.rc")) errmem;
  if (!stralloc_0(&dst)) errmem;
  if (stat(dst.s,&st) != 0) {
    errno = 0;                         /* reset errno after stat */
    stralloc_init(&src);
    if (!stralloc_copys(&src,home)) errmem;
    if (!stralloc_cats(&src,"/etc/template-qemud.rc")) errmem;
    if (!stralloc_0(&src)) errmem;
    stralloc_init(&sa);
    if (!stralloc_copys(&sa,"cat ")) errmem;
    if (!stralloc_cats(&sa,src.s)) errmem;
    if (!stralloc_cats(&sa," | sed \"s/[<]name[>]/")) errmem;
    if (!stralloc_cats(&sa,name.s)) errmem;
    if (!stralloc_cats(&sa,"\"/g")) errmem;
    if (!stralloc_cats(&sa," | sed \"s/[<]name-sda[>]/")) errmem;
    if (!stralloc_cats(&sa,lnam.s)) errmem;
    if (!stralloc_cats(&sa,"-sda\"/g")) errmem;
    if (!stralloc_cats(&sa," > ")) errmem;
    if (!stralloc_cats(&sa,dst.s)) errmem;
    if (!stralloc_0(&sa)) errmem;
    createFile(&sa);
  }

  /* create the '*-qemu.cfg' file from 'etc/template-qemu.cfg' */
  if (!stralloc_copys(&dst,lnam.s)) errmem;
  if (!stralloc_cats(&dst,"-qemu.cfg")) errmem;
  if (!stralloc_0(&dst)) errmem;
  if (stat(dst.s,&st) != 0) {
    errno = 0;                       /* reset errno after stat */
    stralloc_init(&src);
    if (!stralloc_copys(&src,home)) errmem;
    if (!stralloc_cats(&src,"/etc/template-qemu.cfg")) errmem;
    if (!stralloc_0(&src)) errmem;
    stralloc_init(&sa);
    if (!stralloc_copys(&sa,"cp ")) errmem;
    if (!stralloc_cats(&sa,src.s)) errmem;
    if (!stralloc_cats(&sa," ")) errmem;
    if (!stralloc_cats(&sa,dst.s)) errmem;
    if (!stralloc_0(&sa)) errmem;
    createFile(&sa);
  }

  /* create the '*-sda.img' image file with default parameters */
  if (!stralloc_copys(&dst,lnam.s)) errmem;
  if (!stralloc_cats(&dst,"-sda.img")) errmem;
  if (!stralloc_0(&dst)) errmem;
  if (stat(dst.s,&st) != 0) {
    errno = 0;                       /* reset errno after stat */
    stralloc_init(&sa);
    if (!stralloc_copys(&sa, "qemu-img create -f qcow2 ")) errmem;
    if (!stralloc_cats(&sa,dst.s)) errmem;
    if (!stralloc_cats(&sa," 30G")) errmem;
    if (!stralloc_0(&sa)) errmem;
    createFile(&sa);
  }
//_exit(0);  /* debug */
  /* last step: write it into registered file */
  stralloc_init(&sa);
  if (!stralloc_copys(&sa,home)) errmem;
  if (!stralloc_cats(&sa,"/etc")) errmem;
  if (!stralloc_0(&sa)) errmem;
  if (chdir(sa.s) == -1) errint(errno,B("Unable to switch to '",home,"/etc'!"));
  fd = open_append(fn);
  if (fd < 0) errint(errno,B("Couldn't open ",fn,"!"));
  buffer_init(&b,write,fd,buf,sizeof(buf));
  buffer_puts(&b,path.s);
  buffer_puts(&b,"/");
  buffer_puts(&b,name.s);
  buffer_puts(&b,"\n");
  mywrite(fd,buf,sizeof(buf));
  close(fd);

  log(B("Successfully registered ",name.s,"."));
  return(0);
}
