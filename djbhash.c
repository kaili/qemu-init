/********************************************************************************
 * djbhash - linkable object file                                               *
 *                                                                              *
 *  Author: Kai Peter, ©2020                                                    *
 * License: ISC                                                                 *
 * Version: 0.1.1                                                               *
 *                                                                              *
 * Description: Implementation of the hash function from D.J. Bernstein.        *
 *******************************************************************************/
#include <unistd.h>

unsigned long djbhash(char *str) {
  unsigned long hash = 593851319;  /* djb used '5381' */
  int c;
  while ((c = *str++))
    hash = ((hash << 5) + hash) + c; /* hash * 33 + c */
    /* alternative (now prefered by djb: */
    //hash = (hash * 33) ^ str[c];
  return hash;
}
