#ifndef VNCPORTS_H
#define VNCPORTS_H

#include "uint_t.h"

extern int initvnc();
extern int getvncport(uint16 p);
extern int setvncport(uint16 p);

extern unsigned int vncport;

#endif
