/********************************************************************************
 * changeowner (linkable object file)                                           *
 *                                                                              *
 *  Author: Kai Peter, ©2020-                                                   *
 * License: ISC                                                                 *
 * Version: 0.3                                                                 *
 *                                                                              *
 * Description: Changing the ownership of files requires  privilege escalation  *
 *              for ordinary users, thus it is easier to use 'sudo' in a shell  *
 *              instead of the syscall. Using 'execlp' prevents the definition  *
 *              of hardcoded pathes.                                            *
 *              Parameters: filename, user[.group] (group is optional)          *
 *              Returns 0 on success or -1 otherwise.                           *
 *******************************************************************************/
#define ME ""

#include <unistd.h>
#include "qstrings.h"
#include "wait.h"

#include "changeowner.h"

int chgOwner(char *f,const char *u) {
  int pid;
  int wstat;
  stralloc t = {0};

  if (str_len(u) == 0) u = sysusr;
  pid = fork();
  if (!pid) {
    if (!stralloc_copys(&t,"sudo chown ")) errmem;
    if (!stralloc_cats(&t,u)) errmem;
    if (!stralloc_cats(&t," ")) errmem;
    if (!stralloc_cats(&t,f)) errmem;
    if (!stralloc_0(&t)) errmem;
    /* The error handling is for 'execlp', NOT for errors of shell commands */
    if(execlp("sh", "sh", "-c", t.s, (char *) NULL) < 0) return(-1);
  }
  wait_pid(&wstat,pid);
  return(0);
}
