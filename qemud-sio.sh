#********************************************************************************
# qemud-sio (executable library)                                                *
#                                                                               *
#  Author: Kai Peter, ©2021-present                                             *
# Version: 0.3                                                                  *
# License: ISC                                                                  *
#                                                                               *
# Description: Bidirectional (unix) socket connections with the qemu monitor.   *
#********************************************************************************
VERSION=`cat $0 | grep -m 1 'Version' | cut -d\   -f3`       # get version number
ME=`basename $0`                                             # set program name

PATH=QLIBDIR:$PATH
RUNDIR="QRUNDIR"
declare -i DBG=0

usage() { echo "Usage: "$ME" <option> <name>"; exit 0 ; }

command() {
  echo "$MONCMD" | socat - unix-connect:"$RUNDIR/$NAME"-sock.ctl \
       | grep -v '^(qemu)' | grep -v ^QEMU
  exit $?     # usually with code 1
}

monitor() {
  echo "Connecting to qemu monitor ..."
  printf "\033[1mUse CTRL-C to exit the monitor!\033[0m\n"
  socat -,echo=0,icanon=0 unix-connect:"$RUNDIR/$NAME"-sock.mon; echo
  exit $?
}

[ "$1" ] || usage
for OPT in "$@" ; do
  case "$OPT" in
       -m|--mon) ACTION="monitor";;
       -c|--cmd) MONCMD="$2" && ACTION="command" ; shift;;
   -V|--version) echo "$ME-$VERSION" ; exit 0;;
      -h|--help) usage;;
              *) NAME=`echo $OPT | tr '[A-Z]' '[a-z]'`;;
  esac
  shift
done
"$ACTION"
