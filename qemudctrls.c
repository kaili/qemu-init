/********************************************************************************
 * qemudctrls (linkable object file)                                            *
 *                                                                              *
 *  Author: Kai Peter, ©2020-                                                   *
 * License: ISC                                                                 *
 * Version: 0.4                                                                 *
 *                                                                              *
 * Description: Read/set the configuration parameters for qemud/qemud-init from *
 *              file <name>-qemud.rc (qemud runtime controls).                  *
 *******************************************************************************/
#include <unistd.h>
#include <sys/stat.h>
#include "buffer.h"
#include "qstrings.h"
#include "scan.h"

#include "qemudctrls.h"
#include "convertname.h"

unsigned int stopdelay;
unsigned int bootdelay;
stralloc precmd  = {0};
stralloc postcmd = {0};

/* Control parameter for 'qemud-init' */
void getControls(char *key, char *value) {

  /* make sure variables are initialized if doesn't exist in old rc-files */
  if (precmd.len == 0) if (!stralloc_0(&precmd)) errmem;
  if (postcmd.len == 0) if (!stralloc_0(&postcmd)) errmem;

  if (strequal(key,"BOOTDELAY")) {
    scan_ulong(value,(unsigned long *)&bootdelay);
  }
  if (strequal(key,"STOPDELAY")) {
    scan_ulong(value,(unsigned long *)&stopdelay);
  }

  if (strequal(key,"PRECMD")) {
    if (strequal(value,"")) return;
    if (!stralloc_copys(&precmd,value)) errmem;
    if (!stralloc_0(&precmd)) errmem;
  }

  if (strequal(key,"POSTCMD")) {
    if (strequal(value,"")) return;
    if (!stralloc_copys(&postcmd,value)) errmem;
    if (!stralloc_0(&postcmd)) errmem;
  }
  return;
}
