/********************************************************************************
 * vncports (linkable object file)                                              *
 *                                                                              *
 *  Author: Kai Peter, ©2021-<present>                                          *
 * License: ISC                                                                 *
 * Version: 0.2                                                                 *
 *                                                                              *
 * Description: Handle VNC ports.                                               *
 *******************************************************************************/
#define ME ""

#include <unistd.h>
#include <sys/stat.h>
#include "ip.h"
#include "qsocket.h"
#include "qstrings.h"
#include "readclose.h"
#include "scan.h"

#include "isrunning.h"
#include "vncports.h"

#include "layout.h"

int initvnc(char *n) {
  stralloc fn = {0};
  stralloc sa = {0};
  int i;
  struct stat st;
  unsigned int p = 0;

  if(!stralloc_copys(&fn,etcdir)) errmem;
  if(!stralloc_cats(&fn,"/vncoffset")) errmem;
  if(!stralloc_0(&fn)) errmem;
  if (stat(fn.s,&st) == 0) {
    if (!openreadclose(fn.s,&sa,64)) errmem;;
    i = instr(sa.s,"\n") - 1;
    stralloc_copys(&sa,strsplit(sa.s,i,'L'));
    scan_ulong(sa.s,(unsigned long *)&p);
    if (p > 65536) p = 0;  /* perhaps users fault */
  }
  if (p == 0) p = 50000;    /* default (fallback) */

  if (IsRunning(n)) {
     p = getvncport(p);
  } else {
     p = setvncport(p);
  }
  /* if setvncports returns with '-1' then p is greater than 65536 */
  if (p > 65536) errint(ESOFT,"Couldn't get a VNC port!");

  return(p);
}

int getvncport(uint16 p) {
  stralloc sa = {0};
  int i;

  i = instr(proccmd.s,"-display vnc=:");  /* exist here always */
  if (!stralloc_copys(&sa,strsplit(proccmd.s,i,'R'))) errmem;
  i = instr(sa.s,":") -1;
  if (!stralloc_copys(&sa,strsplit(sa.s,i,'R'))) errmem;
  scan_ulong(sa.s,(unsigned long *)&p);
  return(p);
}

int setvncport(uint16 p) {
  char ip[16] = {0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0};
  int s;

  p = p + 5900;

  ip6_scan("127.0.0.1",ip);  // ipv4
//  ip6_scan("localhost",ip);  // ipv4
  while(1) {
    s = socket_tcp();
    if(qbind(s,ip,p,0) == 0) { close(s); return(p - 5900); }
    p++;
  }
  return(-1);
}
