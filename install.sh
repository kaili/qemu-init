#********************************************************************************
echo -e "\033[1mInstalling qemu-init ...\033[0m"         # start the installation

_chown() { chown -f $1 $2 || ERRCHOWN=1 ; }

# directories defined in conf-build
PREFIX="$DESTDIR""QPREFIX"
BINDIR="$DESTDIR""QBINDIR"
LIBDIR="$DESTDIR""QLIBDIR"
MANDIR="$DESTDIR""QPREFIX/man"
ETCDIR="$DESTDIR""QETCDIR"
VARDIR="$DESTDIR""QVARDIR"
DATDIR="$DESTDIR""QDATDIR"
RUNDIR="$DESTDIR""QRUNDIR"

USR=QUSR
GRP=QGRP

# Install user(s) and group(s)
###mkUsrGrp###                     # placeholder for user/group functions
[ -f ac-mkusrgrp ] && [ `type -t mkUsrGrp` ] && mkUsrGrp
[ $(type -pa `head -1 ac-pwlockusr | cut -d' ' -f1`) ] && \
  USRLOCKCMD=`head -1 ac-pwlockusr` && $USRLOCKCMD $USR

HOMEDIR="$DESTDIR""QHOMEDIR"
[ -d $HOMEDIR ] || mkdir -p -m 0750 $HOMEDIR
chmod 01750 "$HOMEDIR"
_chown $USR:$GRP $HOMEDIR

[ -d "$BINDIR" ] || mkdir -pv "$BINDIR"
[ -d "$LIBDIR" ] || mkdir -pv "$LIBDIR"
# the data dir should (have to) be in qemud's home always
mkdir -pv -m 3775 "$DATDIR"; _chown "$USR":"$GRP" "$DATDIR"
# don't touch existing (FHS) directories
[ -d "$ETCDIR" ] || mkdir -pv -m 3775 "$ETCDIR"
[ -d "$VARDIR" ] || mkdir -pv -m 2775 "$VARDIR"
[ -d "$RUNDIR" ] || mkdir -pv -m 3775 "$RUNDIR"
_chown "$USR":"$GRP" "$ETCDIR"
_chown "$USR":"$GRP" "$VARDIR"
_chown "$USR":"$GRP" "$RUNDIR"

cp -v qemud-init   "$LIBDIR"; _chown 0:"$GRP" "$LIBDIR"/qemud-init
cp -v qemud-acl    "$LIBDIR"; _chown 0:"$GRP" "$LIBDIR"/qemud-acl
cp -v qemud-ast    "$LIBDIR"; _chown 0:"$GRP" "$LIBDIR"/qemud-ast
cp -v qemud-cfg    "$LIBDIR"; _chown 0:"$GRP" "$LIBDIR"/qemud-cfg
cp -v qemud-reg    "$LIBDIR"; _chown 0:"$GRP" "$LIBDIR"/qemud-reg
cp -v qemud-sio    "$LIBDIR"; _chown 0:"$GRP" "$LIBDIR"/qemud-sio
cp -v qemud-tap    "$LIBDIR"; _chown 0:"$GRP" "$LIBDIR"/qemud-tap
cp -v qemud-vnc    "$LIBDIR"; _chown 0:"$GRP" "$LIBDIR"/qemud-vnc
cp -v vncconnect   "$LIBDIR"; _chown 0:"$GRP" "$LIBDIR"/vncconnect
cp -dv qemud-br*    "$LIBDIR"

cp -v qemu-adm     "$BINDIR"; _chown 0:"$GRP" "$BINDIR"/qemu-adm
cp -v qemud        "$BINDIR"; _chown 0:"$GRP" "$BINDIR"/qemud

###symInstall###           # placeholder for standard function to create symlinks

# config files:
cp -nv registered.tpl    "$ETCDIR/registered"
cp -nv vncstatus.tpl     "$ETCDIR/vncstatus"
cp -nv vncoffset.tpl     "$ETCDIR/vncoffset"
chmod -f 0664 "$ETCDIR/registered" "$ETCDIR/vncoffset" "$ETCDIR/vncstatus"
# (hard)link etc/vncstatus to var/vncstatus
[ ! -f $VARDIR/vncstatus ] && ln $ETCDIR/vncstatus $VARDIR/vncstatus
# these files should be owned by root always
_chown 0 "$ETCDIR/registered $ETCDIR/vncoffset $ETCDIR/vncstatus"
# templates:
cp -v template-qemud.rc  "$ETCDIR"
cp -v template-qemu.cfg  "$ETCDIR"

# optional install's
SUDOERSDIR="$DESTDIR""QSUDOINCDIR"
# do not create the sudoers include dir except for a dummy (destdir) installation
[ $DESTDIR ] && mkdir -pv $SUDOERSDIR
# here we overwrite the destination file, because of possible changes
[ -d "$SUDOERSDIR" ] && cp -v sudo-qemud "$SUDOERSDIR/qemud" || \
  printf "\033[91mWarning: Unable to install the sudoers file!\033[0m\n"

# the $HOME/.ssh file (example) - have to edited anyway
mkdir -p -m 0700 $HOMEDIR/.ssh
cp -nv dot-ssh-qemud $HOMEDIR/.ssh/authorized_keys
_chown $USR $HOMEDIR/.ssh/authorized_keys

# man pages
###manInstall###         # placeholder for man page installation default function

# delete files which are removed from package (silently)
rm -f "$ETCDIR/portsinit"
rm -f "$LIBDIR/qemud-cmd"
rm -f "$LIBDIR/qemud-mon"
rm -f "$LIBDIR/qtcpclient"
rm -f "$LIBDIR/qtcpconn-io"
rm -f "$MANDIR/man8/qemud-cmd.8"
rm -f "$MANDIR/man8/qemud-mon.8"

echo -e "\033[1m ... finished!\033[0m"
[ "$ERRCHOWN" = "1" ] && printf "\033[93m%s\033[0m\n" \
  "Warning: ownership of some files was not set! (root privileges required)"
